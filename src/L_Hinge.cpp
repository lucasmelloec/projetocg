///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Hinge.h"
#include "L_Convex.h"

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Hinge::className[] = "Hinge";

Luna<L_Hinge>::RegType L_Hinge::Register[] = {
	method(L_Hinge, L_Destroy, "Destroy"),
    method(L_Hinge, L_SetMotor, "SetMotor"),
    method(L_Hinge, L_DisableMotor, "DisableMotor"),
    method(L_Hinge, L_SetLimits, "SetLimits"),
    method(L_Hinge, L_GetAngle, "GetAngle"),
    method(L_Hinge, L_SetAngleMotor, "EnableAngleMotor"),
    method(L_Hinge, L_SetAngleMotorTarget, "SetAngleMotorTarget"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Hinge::L_Hinge() : L_Object(){
    hingeConstraint = NULL;
}

L_Hinge::~L_Hinge(){
}

// Lua side constructors

L_Hinge::L_Hinge(lua_State * L) : L_Object(L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 14){
			lua_pushstring(L, "Hinge:Hinge() accepts fourteen arguments.");
			lua_error(L);

			return;
	}
#endif

#if LUA_CHECK_ERRORS
    if(!lua_istable(L, 1) || !lua_istable(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4) || !lua_isnumber(L, 5) || !lua_isnumber(L, 6) || !lua_isnumber(L, 7) || !lua_isnumber(L, 8) || !lua_isnumber(L, 9) || !lua_isnumber(L, 10) || !lua_isnumber(L, 11) || !lua_isnumber(L, 12) || !lua_isnumber(L, 13) || !lua_isnumber(L, 14)){
			lua_pushstring(L, "Hinge:Hinge() was called with wrong arguments.");
			lua_error(L);

			return;
		}
#endif

    // (Convex A in lua) (Convex B in lua) pivota_x pivota_y pivota_z pivotb_x pivotb_y pivotb_z axisa_x axisa_y axisa_z axisb_x axisb_y axisb_z <- 14
    lua_pushinteger(L, 0);
    // (Convex A in lua) (Convex B in lua) pivota_x pivota_y pivota_z pivotb_x pivotb_y pivotb_z axisa_x axisa_y axisa_z axisb_x axisb_y axisb_z (0)<- 15
    lua_gettable(L, 1);

    lua_pushinteger(L, 0);
    lua_gettable(L, 2);
    // (Convex A in lua) (Convex B in lua) pivota_x pivota_y pivota_z pivotb_x pivotb_y pivotb_z axisa_x axisa_y axisa_z axisb_x axisb_y axisb_z (L_Convex A in c++) (L_Convex B in c++)
    L_Convex** convexA = static_cast<L_Convex**>(luaL_checkudata(L, 15, L_Convex::className));
    L_Convex** convexB = static_cast<L_Convex**>(luaL_checkudata(L, 16, L_Convex::className));

    float pivotA_x = (float)lua_tonumber(L, 3);
    float pivotA_y = (float)lua_tonumber(L, 4);
    float pivotA_z = (float)lua_tonumber(L, 5);

    float pivotB_x = (float)lua_tonumber(L, 6);
    float pivotB_y = (float)lua_tonumber(L, 7);
    float pivotB_z = (float)lua_tonumber(L, 8);

    float axisA_x = (float)lua_tonumber(L, 9);
    float axisA_y = (float)lua_tonumber(L, 10);
    float axisA_z = (float)lua_tonumber(L, 11);

    float axisB_x = (float)lua_tonumber(L, 12);
    float axisB_y = (float)lua_tonumber(L, 13);
    float axisB_z = (float)lua_tonumber(L, 14);

    lua_pop(L, 16);

    hingeConstraint = new btHingeConstraint(*((*convexA)->rigidBody),(*(*convexB)->rigidBody),
        btVector3(pivotA_x, pivotA_y, pivotA_z), btVector3(pivotB_x, pivotB_y, pivotB_z),
        btVector3(axisA_x, axisA_y, axisA_z), btVector3(axisB_x, axisB_y, axisB_z),
        true);

    BulletPhysics::dynamicsWorld->addConstraint(hingeConstraint);

    (*convexA)->connectedParts.push_back(hingeConstraint);
}

// Post-constructor / Pre-destructor

void L_Hinge::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Hinge::DestroyObject(lua_State * L) {
	if(Changed!=NULL) {
        Changed->Destroy(L);
        Changed = NULL;
    }
    if(hingeConstraint != NULL){
        BulletPhysics::dynamicsWorld->removeConstraint(hingeConstraint);
        delete hingeConstraint;
        hingeConstraint = NULL;
    }
}

// Exposed Methods

int L_Hinge::L_Destroy(lua_State *L){
    DestroyObject(L);

    return 0;
}

int L_Hinge::L_SetMotor(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 3){
			lua_pushstring(L, "Hinge:SetMotor() accepts two arguments.");
			lua_error(L);

			return 0;
	}
#endif

#if LUA_CHECK_ERRORS
    if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3)){
			lua_pushstring(L, "Hinge:SetMotor() was called with wrong arguments.");
			lua_error(L);

			return 0;
		}
#endif

    float velocity = (float)lua_tonumber(L, 2);
    float impulse = (float)lua_tonumber(L, 3);

    hingeConstraint->getRigidBodyA().activate();
    hingeConstraint->getRigidBodyB().activate();

    hingeConstraint->enableAngularMotor(true, velocity, impulse);

    return 0;
}

int L_Hinge::L_DisableMotor(lua_State *L)
{
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 1){
			lua_pushstring(L, "Hinge:DisableMotor() doesn't accept arguments.");
			lua_error(L);

			return 0;
	}
#endif

    hingeConstraint->enableAngularMotor(true, 0.0f, hingeConstraint->getMaxMotorImpulse());

    return 0;
}

int L_Hinge::L_SetLimits(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 3){
            lua_pushstring(L, "Hinge:SetLimits() accepts only two arguments, which are the lower and upper angle limits.");
            lua_error(L);

            return 0;
    }
#endif

    hingeConstraint->setLimit(lua_tonumber(L, 2), lua_tonumber(L, 3));

    return 0;
}

int L_Hinge::L_GetAngle(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 1){
            lua_pushstring(L, "Hinge:GetAngle() doesn't take arguments.");
            lua_error(L);

            return 0;
    }
#endif

    lua_pop(L, 1);
    lua_pushnumber(L, (float)hingeConstraint->getHingeAngle());

    return 1;
}

int L_Hinge::L_SetAngleMotor(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 2){
            lua_pushstring(L, "Hinge:SetAngleMotor() accepts one arguments.");
            lua_error(L);

            return 0;
    }
#endif

#if LUA_CHECK_ERRORS
    if(!lua_isnumber(L, 2)){
            lua_pushstring(L, "Hinge:SetAngleMotor() was called with wrong arguments.");
            lua_error(L);

            return 0;
        }
#endif

    float maxImpulse = (float)lua_tonumber(L, 2);

    hingeConstraint->getRigidBodyA().activate();
    hingeConstraint->getRigidBodyB().activate();

    hingeConstraint->enableMotor(true);
    hingeConstraint->setMaxMotorImpulse(maxImpulse);

    return 0;
}

int L_Hinge::L_SetAngleMotorTarget(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 2){
            lua_pushstring(L, "Hinge:SetAngleMotorTarget() accepts one arguments.");
            lua_error(L);

            return 0;
    }
#endif

#if LUA_CHECK_ERRORS
    if(!lua_isnumber(L, 2)){
            lua_pushstring(L, "Hinge:SetAngleMotorTarget() was called with wrong arguments.");
            lua_error(L);

            return 0;
        }
#endif

    float targetAngle = (float)lua_tonumber(L, 2);

    hingeConstraint->getRigidBodyA().activate();
    hingeConstraint->getRigidBodyB().activate();

    hingeConstraint->setMotorTarget(targetAngle, 0.2);
    return 0;
}
