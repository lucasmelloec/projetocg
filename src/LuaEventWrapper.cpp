///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "LuaEventWrapper.h"

LuaEvent::LuaEvent(lua_State * L, const char * name){
	lua_getglobal(L, "glib");
	// (glib)
	lua_pushstring(L, "Event");
	// (glib) "Event"
	lua_gettable(L, -2);
	// (glib) (Event)
	lua_pushstring(L, "new");
	// (glib) (Event) "new"
	lua_gettable(L, -2);
	// (glib) (Event) (new)
	lua_pushvalue(L, -2);
	// (glib) (Event) (new) (Event)
	lua_pcall(L, 1, 1, 0);
	// (glib) (Event) (resultingEvent)
	lua_remove(L, -2);
	lua_remove(L, -2);
	// (resultingEvent)
	objstoreHash = objstoreCurrent++;
	lua_getglobal(L, "objstore");
	// (resultingEvent) (objstore)
	lua_pushinteger(L, objstoreHash);
	// (resultingEvent) (objstore) objstoreHash
	lua_pushvalue(L, -3);
	// (resultingEvent) (objstore) objstoreHash (resultingEvent)
	lua_settable(L, -3);
	// (resultingEvent) (objstore)
	lua_pop(L, 1);
	// (resultingEvent)
	lua_pushstring(L, name);
	// (resultingEvent) (name)
	lua_insert(L, -2);
	lua_settable(L, -3);
}

void LuaEvent::Fire(lua_State * L, int (*callback)(lua_State *)){
	lua_getglobal(L, "objstore");
	lua_pushinteger(L, objstoreHash);
	lua_gettable(L, -2);

	// (objstore) (Event)

	lua_pushstring(L, "fireParallel");
	lua_gettable(L, -2);

	// (objstore) (Event) (fireParallel)

	lua_pushvalue(L, -2);

	// (objstore) (Event) (fireParallel) (Event)

	int narg = callback(L);

	// (objstore) (Event) (fireParallel) (Event) <other_args>

	lua_pcall(L, narg+1, 0, 0);

	// (objstore) (Event)

	lua_pop(L, 2);
}

void LuaEvent::Fire(lua_State * L, int argn){
	lua_getglobal(L, "objstore");
	lua_pushinteger(L, objstoreHash);
	lua_gettable(L, -2);

	// (objstore) (Event)

	lua_pushstring(L, "fireParallel");
	lua_gettable(L, -2);

	// (objstore) (Event) (fireParallel)

	lua_pushvalue(L, -2);

	// (objstore) (Event) (fireParallel) (Event)

	for(int i=0; i<argn; i++){
		//cout << -(4+argn) << " / " << lua_gettop(L) << endl;
		lua_pushvalue(L, -(4+argn));
	}

	// (objstore) (Event) (fireParallel) (Event) <other_args>

	lua_pcall(L, argn+1, 0, 0);

	// (objstore) (Event)

	lua_pop(L, 2+argn);
}

void LuaEvent::Destroy(lua_State * L){
	lua_getglobal(L, "objstore");
	lua_pushinteger(L, objstoreHash);
	lua_pushnil(L);
	lua_settable(L, -3);
	lua_pop(L, 1);

	/*int * a = (int *)malloc(sizeof(int)), b;
	for(int i=0;; i++){
		b=b+a[i];
	}*/
}
