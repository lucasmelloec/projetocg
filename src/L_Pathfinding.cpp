///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Pathfinding.h"
#include <map>
#include <queue>

#include "tiny_obj_loader.h"

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) > (b)) ? (b) : (a))

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Pathfinding::className[] = "Pathfinding";

Luna<L_Pathfinding>::RegType L_Pathfinding::Register[] = {
	method(L_Pathfinding, L_AddNavMesh, "AddNavMesh"),
	method(L_Pathfinding, L_GetWaypoints, "GetWaypoints"),
	method(L_Pathfinding, L_GetNearestTriangleDistance, "GetNearestTriangleDistance"),
	{0,0}
};

//== EXPOSED CLASS FUNCTIONS ===================================

class point{
	public:
	point (float p_x,float p_y) : x(p_x),y(p_y){}

	float x;
	float y;
};

int intersect(point a,point b,point c,point d){
	float den = ((d.y-c.y)*(b.x-a.x)-(d.x-c.x)*(b.y-a.y));
	float num1 = ((d.x - c.x)*(a.y-c.y) - (d.y- c.y)*(a.x-c.x));
	float num2 = ((b.x-a.x)*(a.y-c.y)-(b.y-a.y)*(a.x-c.x));
	float u1 = num1/den;
	float u2 = num2/den;

	if (den == 0 && num1  == 0 && num2 == 0)
	 /* The two lines are coincidents */
	 return 0;
	if (den == 0)
	 /* The two lines are parallel */
	 return 0;
	if (u1 <0 || u1 > 1 || u2 < 0 || u2 > 1)
	 /* Lines do not collide */
	 return 0;
	/* Lines DO collide */
	return 1;
}

#define NO 0
#define YES 1
#define COLLINEAR 2

int areIntersecting(
    float v1x1, float v1y1, float v1x2, float v1y2,
    float v2x1, float v2y1, float v2x2, float v2y2
) {
    float d1, d2;
    float a1, a2, b1, b2, c1, c2;

    // Convert vector 1 to a line (line 1) of infinite length.
    // We want the line in linear equation standard form: A*x + B*y + C = 0
    // See: http://en.wikipedia.org/wiki/Linear_equation
    a1 = v1y2 - v1y1;
    b1 = v1x1 - v1x2;
    c1 = (v1x2 * v1y1) - (v1x1 * v1y2);

    // Every point (x,y), that solves the equation above, is on the line,
    // every point that does not solve it, is not. The equation will have a
    // positive result if it is on one side of the line and a negative one
    // if is on the other side of it. We insert (x1,y1) and (x2,y2) of vector
    // 2 into the equation above.
    d1 = (a1 * v2x1) + (b1 * v2y1) + c1;
    d2 = (a1 * v2x2) + (b1 * v2y2) + c1;

    // If d1 and d2 both have the same sign, they are both on the same side
    // of our line 1 and in that case no intersection is possible. Careful,
    // 0 is a special case, that's why we don't test ">=" and "<=",
    // but "<" and ">".
    if (d1 > 0 && d2 > 0) return NO;
    if (d1 < 0 && d2 < 0) return NO;

    // The fact that vector 2 intersected the infinite line 1 above doesn't
    // mean it also intersects the vector 1. Vector 1 is only a subset of that
    // infinite line 1, so it may have intersected that line before the vector
    // started or after it ended. To know for sure, we have to repeat the
    // the same test the other way round. We start by calculating the
    // infinite line 2 in linear equation standard form.
    a2 = v2y2 - v2y1;
    b2 = v2x1 - v2x2;
    c2 = (v2x2 * v2y1) - (v2x1 * v2y2);

    // Calculate d1 and d2 again, this time using points of vector 1.
    d1 = (a2 * v1x1) + (b2 * v1y1) + c2;
    d2 = (a2 * v1x2) + (b2 * v1y2) + c2;

    // Again, if both have the same sign (and neither one is 0),
    // no intersection is possible.
    if (d1 > 0 && d2 > 0) return NO;
    if (d1 < 0 && d2 < 0) return NO;

    // If we get here, only two possibilities are left. Either the two
    // vectors intersect in exactly one point or they are collinear, which
    // means they intersect in any number of points from zero to infinite.
    if ((a1 * b2) - (a2 * b1) == 0.0f) return COLLINEAR;

    // If they are not collinear, they must intersect in exactly one point.
    return YES;
}

#if USE_POLYGON_SEGMENTATION
Polygon * L_Pathfinding::MatchPolygonEdge(core::vector3df vx1, core::vector3df vx2){
	vector<Polygon *> vx1list;

	if(pmap.find(vx1)!=pmap.end())
		vx1list = (vector<Polygon *>)(pmap[vx1]);

	unsigned int i, j;

	for(i=0; i<vx1list.size(); i++){
		for(j=0; j<(unsigned int)vx1list[i]->size; j++){
			if(vx1list[i]->vx[j].equals(vx2)){
				// cout << "Matched!" << endl;
				return vx1list[i];
			}
		}
	}

	return NULL;
}
#endif
// #else
Triangle * L_Pathfinding::MatchTriangleEdge(core::vector3df vx1, core::vector3df vx2){
	vector<Triangle *> vx1list;

	if(pmap.find(vx1)!=pmap.end())
		vx1list = pmap[vx1];
	else
		return NULL;

	unsigned int i;

	for(i=0; i<vx1list.size(); i++){
		if(
			vx1list[i]->vx1.equals(vx2) ||
			vx1list[i]->vx2.equals(vx2) ||
			vx1list[i]->vx3.equals(vx2)
			){
			return vx1list[i];
		}
	}

	return NULL;
}
// #endif

#define INTERSECT_METHOD 1

bool Polygon::isIn(const float x, const float y){
	if(!hasBounds)
		calculateBounds();

	int i;

	int intersectCount = 0;

	for(i=0; i<size; i++){
		int n = (i+1)%size;

		#if INTERSECT_METHOD == 0
		int doesIntersect = intersect(
			point(minX-1.0f, minZ-1.0f),
			point(x, y),
			point(vx[i].X, vx[i].Z),
			point(vx[n].X, vx[n].Z)
			);
		if(doesIntersect)
			intersectCount++;
		#else
		if(areIntersecting(
			minX-0.3216958f, minZ-0.2132679f,
			x, y,
			vx[i].X, vx[i].Z,
			vx[n].X, vx[n].Z
			)==YES)
			intersectCount++;
		#endif
	}

	return (intersectCount % 2) == 1 ? true : false;
}

bool Polygon::isIn(const core::vector3df & pos){
	return isIn(pos.X, pos.Z);
}

void Polygon::calculateBounds(){
	minX =  200000;
	minZ =  200000;

	maxX = -200000;
	maxZ = -200000;

	hasBounds = true;

	for(int i=0; i<size; i++){
		if(vx[i].X < minX)
			minX = vx[i].X;
		if(vx[i].X > maxX)
			maxX = vx[i].X;
		if(vx[i].Z < minZ)
			minZ = vx[i].Z;
		if(vx[i].Z > maxZ)
			maxZ = vx[i].Z;
	}
}

core::vector3df Polygon::getCenter(){
	core::vector3df ret = core::vector3df(0.f, 0.f, 0.f);

	int i;
	for(i=0; i<size; i++){
		ret.X += vx[i].X;
		ret.Y += vx[i].Y;
		ret.Z += vx[i].Z;
	}

	float mult = 1.f / (float)size;

	ret.X *= mult;
	ret.Y *= mult;
	ret.Z *= mult;

	return ret;
}

core::vector3df Polygon::getCenterXZ(){
	core::vector3df ret = core::vector3df(0.f, 0.f, 0.f);

	int i;
	for(i=0; i<size; i++){
		ret.X += vx[i].X;
		ret.Z += vx[i].Z;
	}

	float mult = 1.f / (float)size;

	ret.X *= mult;
	ret.Z *= mult;

	return ret;
}

core::vector3df Polygon::fastCenter(){
	if(!hasBounds)
		calculateBounds();

	return core::vector3df((minX+maxX)/2.0f, 0.f, (minZ+maxZ)/2.0f);
}

float Triangle::getDistance(Triangle * t){
	core::vector3df c0 = getCenter();
	core::vector3df c1 = t->getCenter();

	float d1 = c0.X-c1.X;
	float d2 = c0.Z-c1.Z;

	return (d1*d1) + (d2*d2);
}

core::vector3df interp(core::vector3df vx1, core::vector3df vx2, float s){
	return core::vector3df(
		vx1.X*s + vx2.X*(1-s),
		vx1.Y*s + vx2.Y*(1-s),
		vx1.Z*s + vx2.Z*(1-s)
		);
}

void drawLine(core::vector3df vx1, core::vector3df vx2){
	irrmanager->addCubeSceneNode(3, 0, -1, interp(vx1, vx2, 0.33));
	irrmanager->addCubeSceneNode(3, 0, -1, interp(vx1, vx2, 0.66));
}

// core::vector3df L_Pathfinding::Triangle::getCenter(){
// 	return core::vector3df(
// 		(vx1.X + vx2.X + vx3.X) / 3.0f,
// 		(vx1.Y + vx2.Y + vx3.Y) / 3.0f + 0.5f,
// 		(vx1.Z + vx2.Z + vx3.Z) / 3.0f
// 	);
// }

// L_Pathfinding::Triangle::Triangle(){}

core::vector3df bt2irr(btVector3 o){
	return core::vector3df(
		o.x(),
		o.y(),
		o.z()
		);
}

// C++ side constructors

L_Pathfinding::L_Pathfinding() : L_Object(){
	grid = NULL;
}

L_Pathfinding::~L_Pathfinding(){
	// light->remove();
	// lightRoot->remove();
	if(grid!=NULL){
		for(int i=0; i<gridSize; i++){
			delete[] grid[i];
		}
		delete[] grid;
	}

	for(int i=0; i<(int)triangles.size(); i++){
		delete triangles[i];
	}

	triangles.clear();
}

// Lua side constructors

L_Pathfinding::L_Pathfinding(lua_State * L) : L_Object(L){
	Changed = NULL;
	grid = NULL;
}

// Post-constructor / Pre-destructor

void L_Pathfinding::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Pathfinding::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);
}



// Lua binded methods

pair<int, int> L_Pathfinding::getGridFromPoint(float x, float y){
	return make_pair(
		(int)((x - gridStartX) / gridStepX),
		(int)((y - gridStartZ) / gridStepZ)
		);
}

vector<Triangle *> * L_Pathfinding::getGridContent(float x, float y){

	pair<int, int> gridLocation = getGridFromPoint(x, y);

	if(
		gridLocation.first >= 0 &&
		gridLocation.first < gridSize &&
		gridLocation.second >= 0 &&
		gridLocation.second < gridSize
		)
		return &grid[gridLocation.first][gridLocation.second];
	else
		return NULL;
}

Triangle * L_Pathfinding::TriangleFromPoint(float x, float y){
	vector<Triangle *> * candidates = getGridContent(x, y);

	if(candidates==NULL){
		return NULL;
	}else{
		int i;

		for(i=0; i<(int)(*candidates).size(); i++){
			if((*candidates)[i]->isIn(x, y))
				return (*candidates)[i];
		}

		return NULL;
	}
}

Triangle * L_Pathfinding::GetNearestTriangle(float x, float y){
	pair<int, int> gridLocation = getGridFromPoint(x, y);

	if(
		!(gridLocation.first >= 0 &&
		gridLocation.first < gridSize &&
		gridLocation.second >= 0 &&
		gridLocation.second < gridSize)
		){
		gridLocation = make_pair(
			max(0, min(gridLocation.first, gridSize-1)),
			max(0, min(gridLocation.second, gridSize-1))
			);
	}

	int i, j, k, d;

	bool foundTriangles = false;

	Triangle * nearest = NULL;
	float distance = -1.f;

	for(d=0; d<gridSize && !foundTriangles; d++){
		// cout << gridLocation.first << " || " << gridLocation.second << endl;
		// cout << "d: " << d << endl;
		for(i=max(0, gridLocation.first-d); i<=min(gridLocation.first+d, gridSize-1); i++){
		// cout << "\ti: " << i << endl;
			for(j=max(0, gridLocation.second-d); j<=min(gridLocation.second+d, gridSize-1); j++){
		// cout << "\t\tj: " << j << endl;
				if(grid[i][j].size() > 0){
					foundTriangles = true;

					for (k=0; k<(int)grid[i][j].size(); k++)
					{
						if(distance > 0){
							float newDistance = (grid[i][j][k]->getCenterXZ() - core::vector3df(x, 0, y)).getLength();

							if(newDistance < distance){
								nearest = grid[i][j][k];
								distance = newDistance;
							}
						}else{
							nearest = grid[i][j][k];
							distance = (grid[i][j][k]->getCenterXZ() - core::vector3df(x, 0, y)).getLength();
						}
					}
				}
			}
		}
	}

	return nearest;
}

int L_Pathfinding::makeGrid(){
	float minX =  20000;
	float maxX = -20000;

	float minZ =  20000;
	float maxZ = -20000;

	int i, j;

	for(i=0; i<(int)triangles.size(); i++){
		for(j=0; j<triangles[i]->size; j++){
			if(triangles[i]->vx[j].X < minX)
				minX = triangles[i]->vx[j].X;
			if(triangles[i]->vx[j].X > maxX)
				maxX = triangles[i]->vx[j].X;

			if(triangles[i]->vx[j].Z < minZ)
				minZ = triangles[i]->vx[j].Z;
			if(triangles[i]->vx[j].Z > maxZ)
				maxZ = triangles[i]->vx[j].Z;
		}
	}

	minX -= 10.1f;
	minZ -= 10.1f;
	maxX += 10.1f;
	maxZ += 10.1f;

	gridStartX = minX;
	gridStartZ = minZ;

	gridSizeX = maxX - minX;
	gridSizeZ = maxZ - minZ;

	gridStepX = gridSizeX / (float)gridSize;
	gridStepZ = gridSizeZ / (float)gridSize;

	if(grid!=NULL){
		for(i=0; i<gridSize; i++){
			delete[] grid[i];
		}
		delete[] grid;
	}

	// grid = (vector<Triangle *> **)malloc(gridSize*sizeof(vector<Triangle *> *));
	grid = new vector<Triangle *> *[gridSize];
	for(i=0; i<gridSize; i++){
		grid[i] = new vector<Triangle *>[gridSize];
		// grid[i] = (vector<Triangle *> *)malloc(gridSize*sizeof(vector<Triangle *>));
		// for(j=0; j<gridSize; j++){
		// 	grid[i][j] = vector<Triangle *>();
		// }
	}

	int faulty = 0;
	for(i=0; i<(int)triangles.size(); i++){
		core::vector3df center = triangles[i]->getCenter();

		pair<int, int> gridLocation = getGridFromPoint(center.X, center.Z);

		if(
			gridLocation.first >= 0 &&
			gridLocation.first < gridSize &&
			gridLocation.second >= 0 &&
			gridLocation.second < gridSize
			){
			grid[gridLocation.first][gridLocation.second].push_back(triangles[i]);
		}else{
			//cout << "ERROR: " << gridLocation.first << " / " << gridLocation.second << endl;
			faulty++;
		}
	}

	cout << "Faulty: " << faulty << " / " << triangles.size() << endl;
	cout << minX << " / " << maxX << " | " << minZ <<  " / " << maxZ << endl;
	return faulty;
}

int L_Pathfinding::L_GetNearestTriangleDistance(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=3){
			luaL_error(L, "Pathfinding:GetNearestTriangleDistance() takes two arguments.");
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3)){
			lua_pushstring(L, "Pathfinding:GetNearestTriangleDistance() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);

	Triangle * t = GetNearestTriangle(x, y);

	lua_pop(L, 3);

	if(t!=NULL){
		lua_pushnumber(L, (t->getCenterXZ() - core::vector3df(x, 0, y)).getLength());
	}else{
		lua_pushinteger(L, -1);
	}

	return 1;
}

int L_Pathfinding::L_GetWaypoints(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=5){
			luaL_error(L, "Pathfinding:L_GetWaypoints() takes exactly 4 arguments.");
		}
	#endif

	float ox = lua_tonumber(L, 2);
	float oz = lua_tonumber(L, 3);
	float tx = lua_tonumber(L, 4);
	float tz = lua_tonumber(L, 5);

	lua_pop(L, 5);

	Triangle * ot = GetNearestTriangle(ox, oz);
	Triangle * tt = GetNearestTriangle(tx, tz);

	if(ot==NULL||tt==NULL){
		lua_pushboolean(L, false);
		return 1;
	}else{
		vector<float> cost;
		cost.resize(triangles.size(), 200000.f);

		vector<bool> visited;
		visited.resize(triangles.size(), false);

		vector<int> cameFrom;
		cameFrom.resize(triangles.size(), -1);

		cost[ot->id]=0.f;

		class queueElement{
		public:
			int node;

			float cost;
			float eucl;
			float heur;

			queueElement(int n, float x1, float x2) : node(n), cost(x1), eucl(x2), heur(x1+x2){};

			bool operator < (const queueElement & q1) const{
				return cost > q1.cost;
			}
		};

		priority_queue<queueElement> Q;
		Q.push(queueElement(ot->id, 0.0f, ot->getDistance(tt)));

		while(!Q.empty()){
			queueElement top = Q.top();
			Q.pop();

			if(top.node==tt->id)
				break;
			if(visited[top.node])
				continue;
			visited[top.node] = true;
			cost[top.node] = top.cost;

			int i;

			Triangle * ct = triangles[top.node];

			for(i=0; i<(int)G[top.node].size(); i++){
				Triangle * nt = triangles[G[top.node][i]];

				if(cost[nt->id] > top.cost + ct->getDistance(nt)){
					cost[nt->id] = top.cost + ct->getDistance(nt);
					cameFrom[nt->id] = top.node;

					Q.push(queueElement(nt->id, cost[nt->id], nt->getDistance(tt)));
				}

				// drawLine(ct->getCenter(), nt->getCenter());
			}
		}

		if(cameFrom[tt->id]==-1){
			lua_pushboolean(L, false);
			return 1;
		}else{
			int curid = tt->id;

			vector<pair<float, float> > waypoints;
			waypoints.push_back(make_pair(tx, tz));

			while(curid!=ot->id){
				if(curid==-1){
					lua_pushboolean(L, false);
					return 1;
				}

				core::vector3df c = triangles[curid]->getCenter();
				waypoints.push_back(make_pair(c.X, c.Z));

				// irrmanager->addCubeSceneNode(2, 0, -1, triangles[curid]->getCenter());

				curid = cameFrom[curid];
			}

			core::vector3df c = triangles[ot->id]->getCenter();
			waypoints.push_back(make_pair(c.X, c.Z));

			lua_newtable(L);

			int wpid = 1;

			for(int i=(int)waypoints.size()-1; i>=0; i--){
				lua_pushinteger(L, wpid++);

				lua_newtable(L);

				lua_pushstring(L, "x");
				lua_pushnumber(L, waypoints[i].first);
				lua_settable(L, -3);

				lua_pushstring(L, "y");
				lua_pushnumber(L, waypoints[i].second);
				lua_settable(L, -3);

				lua_settable(L, -3);
			}

			return 1;
		}
	}

	return 0;
}

int L_Pathfinding::L_AddNavMesh(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2&&lua_gettop(L)!=3&&lua_gettop(L)!=6&&lua_gettop(L)!=10){
			luaL_error(L, "Map:AddNavMesh() needs at least a filename and whether or not is should be rendered using an octree.");
		}
	#endif

    //scene::IAnimatedMesh* mesh = irrmanager->getMesh(lua_tostring(L, 2));

	// First we get the Lua parameters

    // bool isOctree = lua_toboolean(L, 3);
    float scale = 1;

	btVector3 origin = btVector3(0, 0, 0);
	btQuaternion rotation;
	rotation.setEuler(0, 0, 0);

	if(lua_gettop(L)>=3){
    	scale = lua_tonumber(L, 3);

		if(lua_gettop(L)>=7){
			origin = btVector3(
				lua_tonumber(L, 5),
				lua_tonumber(L, 6),
				lua_tonumber(L, 7)
				);

			if(lua_gettop(L)==11){
				rotation = btQuaternion(
					lua_tonumber(L, 8),
					lua_tonumber(L, 9),
					lua_tonumber(L, 10),
					lua_tonumber(L, 11)
					);
			}
		}
	}

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(origin);
    transform.setRotation(rotation);

	// Next, we read the .OBJ file

	vector<tinyobj::shape_t> obj;
	tinyobj::LoadObj(
		obj,
		lua_tostring(L, 2)
		);

	rescaleShapeT(obj, scale);

	for (int s=0;s<(int)obj.size();s++)
	{
		tinyobj::shape_t& shape = obj[s];
		int faceCount = shape.mesh.indices.size();

		for(int face=0; face<faceCount; face+=3){
			Triangle * t = new Triangle();

			t->vx[0] = bt2irr(transform*btVector3(
				-shape.mesh.positions[shape.mesh.indices[face]*3+0],
				shape.mesh.positions[shape.mesh.indices[face]*3+1],
				shape.mesh.positions[shape.mesh.indices[face]*3+2]
				));
			t->vx[1] = bt2irr(transform*btVector3(
				-shape.mesh.positions[shape.mesh.indices[face+1]*3+0],
				shape.mesh.positions[shape.mesh.indices[face+1]*3+1],
				shape.mesh.positions[shape.mesh.indices[face+1]*3+2]
				));
			t->vx[2] = bt2irr(transform*btVector3(
				-shape.mesh.positions[shape.mesh.indices[face+2]*3+0],
				shape.mesh.positions[shape.mesh.indices[face+2]*3+1],
				shape.mesh.positions[shape.mesh.indices[face+2]*3+2]
				));

			t->id = currentId++;

			triangles.push_back(t);
			G.resize(currentId);

			Triangle * t1 = MatchTriangleEdge(t->vx[0], t->vx[1]);
			Triangle * t2 = MatchTriangleEdge(t->vx[1], t->vx[2]);
			Triangle * t3 = MatchTriangleEdge(t->vx[0], t->vx[2]);

			if(pmap.find(t->vx[0])==pmap.end())
				pmap[t->vx[0]] = std::vector<Triangle *>();

			if(pmap.find(t->vx[1])==pmap.end())
				pmap[t->vx[1]] = std::vector<Triangle *>();

			if(pmap.find(t->vx[2])==pmap.end())
				pmap[t->vx[2]] = std::vector<Triangle *>();

			pmap[t->vx[0]].push_back(t);
			pmap[t->vx[1]].push_back(t);
			pmap[t->vx[2]].push_back(t);

			if(t1!=NULL){
				G[t->id].push_back(t1->id);
				G[t1->id].push_back(t->id);
			}

			if(t2!=NULL){
				G[t->id].push_back(t2->id);
				G[t2->id].push_back(t->id);
			}

			if(t3!=NULL){
				G[t->id].push_back(t3->id);
				G[t3->id].push_back(t->id);
			}
		}
	}


	for(int i=0; i<1; i++){
		if(makeGrid() > 0)
			break;
	}

	return 0;
}
