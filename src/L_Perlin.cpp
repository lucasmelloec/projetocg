///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Perlin.h"
#include "Globals.h"

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Perlin::className[] = "Perlin";

Luna<L_Perlin>::RegType L_Perlin::Register[] = {
	method(L_Perlin, L_Noise, "Noise"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Perlin::L_Perlin() : L_Object(){
}

L_Perlin::~L_Perlin(){
}

// Lua side constructors

L_Perlin::L_Perlin(lua_State * L) : L_Object(L){
}

// Post-constructor / Pre-destructor

void L_Perlin::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Perlin::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);
}

// Lua binded methods


int L_Perlin::L_Noise(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Perlin:Noise() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Perlin:Noise() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	lua_pop(L, 4);

	lua_pushnumber(L, perlin.noise(x, y, z));

	return 1;
}
