///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Object.h"

L_Object & objectRoot;

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Object::className[] = "Object";

Luna<L_Object>::RegType L_Object::Register[] = {
	//method(L_Object, L_Quack, "Quack"),
	{0,0}
};

//==============================================================

int setObjectRoot(lua_State * L){
	L_Object** obj = static_cast<L_Object**>(luaL_checkudata(L, -1, "Object"));
	objectRoot = **obj;

	return 0;
}

L_Object::L_Object(){
	//cout << "Creating L_Object!" << endl;
}

L_Object::~L_Object(){
	if(Changed!=NULL)
		delete Changed;
}

L_Object::L_Object(lua_State *L){

}

void L_Object::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Object::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);
}
