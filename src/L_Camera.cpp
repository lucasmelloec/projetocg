///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Camera.h"
#include "Globals.h"
#include "L_Model.h"

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Camera::className[] = "Camera";

Luna<L_Camera>::RegType L_Camera::Register[] = {
	method(L_Camera, L_SetPosition, "SetPosition"),
	method(L_Camera, L_SetTarget, "SetTarget"),
	method(L_Camera, L_Follow, "Follow"),
	method(L_Camera, L_DebugCamera, "DebugCamera"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Camera::L_Camera() : L_Object(){
    cam = NULL;
    debugCamera = false;
    parentNode = NULL;
}

L_Camera::~L_Camera(){
}

// Lua side constructors

L_Camera::L_Camera(lua_State * L) : L_Object(L){
    cam = irrmanager->addCameraSceneNode();
    debugCamera = false;
    parentNode = NULL;
}

// Post-constructor / Pre-destructor

void L_Camera::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Camera::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);

    if(cam != NULL)
        cam->remove();
}

// Lua binded methods
int L_Camera::L_SetPosition(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 4){
			lua_pushstring(L, "Camera:SetPosition() accepts three arguments.");
			lua_error(L);

			return 0;
	}
#endif

#if LUA_CHECK_ERRORS
    if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Camera:SetPosition() was called with wrong arguments.");
			lua_error(L);

			return 0;
		}
#endif

    float x = (float)lua_tonumber(L, 2);
    float y = (float)lua_tonumber(L, 3);
    float z = (float)lua_tonumber(L, 4);

    camPosition = core::vector3df(x, y, z);

    if(cam != NULL)
        cam->setPosition(core::vector3df(x, y, z));

    return 0;
}

int L_Camera::L_SetTarget(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 4){
			lua_pushstring(L, "Camera:SetTarget() accepts three arguments.");
			lua_error(L);

			return 0;
	}
#endif

#if LUA_CHECK_ERRORS
    if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Camera:SetTarget() was called with wrong arguments.");
			lua_error(L);

			return 0;
		}
#endif

    float x = (float)lua_tonumber(L, 2);
    float y = (float)lua_tonumber(L, 3);
    float z = (float)lua_tonumber(L, 4);

    if(cam != NULL && !debugCamera)
        cam->setTarget(core::vector3df(x, y, z));

    return 0;
}

int L_Camera::L_Follow(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 2){
			lua_pushstring(L, "Camera:Follow() accepts one argument.");
			lua_error(L);

			return 0;
	}
#endif

#if LUA_CHECK_ERRORS
    if(!lua_istable(L, 2)){
			lua_pushstring(L, "Camera:Follow() was called with wrong arguments.");
			lua_error(L);

			return 0;
	}
#endif

    // (Self Object) (Object in lua) <- 2
    lua_pushinteger(L, 0);
    // (Self Object) (Object in lua) (0) <- 3
    lua_gettable(L, 2);
    // (Self Object) (Object in lua) (L_Model in c++) <- 3
    L_Model** parent = static_cast<L_Model**>(luaL_checkudata(L, 3, L_Model::className));

    parentNode = (*parent)->sceneNode;

    if(cam != NULL && !debugCamera) {
        cam->setParent((*parent)->sceneNode);
    }
    return 0;
}

int L_Camera::L_DebugCamera(lua_State *L){
#if LUA_CHECK_ERRORS
    if(lua_gettop(L) != 1){
			lua_pushstring(L, "Camera:DebugCamera() accepts no arguments.");
			lua_error(L);

			return 0;
	}
#endif

    core::vector3df absolutePosition;
    if(cam != NULL) {
        absolutePosition = cam->getAbsolutePosition();

        cam->remove();
    }

    if(debugCamera) {
        cam = irrmanager->addCameraSceneNode(parentNode);
        cam->setPosition(camPosition);
    }
    else {
        cam = irrmanager->addCameraSceneNodeFPS(0, 80.0f, 0.02f);
        cam->setPosition(absolutePosition);
    }

    debugCamera = !debugCamera;

    return 0;
}
