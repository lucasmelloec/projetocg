///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "Globals.h"

Timer globalTimer;

int objstoreCurrent;

scene::ISceneManager * irrmanager;
video::IVideoDriver * irrdriver;
IrrlichtDevice * device;

int LuaRun(lua_State * L){
    int s;

    // execute Lua program
    s = lua_resume(L, NULL, 0);

    if(s!=LUA_OK && s!=LUA_YIELD){
        cout << "=================================================" << endl;
        cout << "Fatal lua error (" << s <<"): " << lua_tostring(L, -1) << endl;
        luaL_traceback(L, L, NULL, 0);
        cout << lua_tostring(L, -1) << endl;
        cout << "=================================================" << endl;
    }

    return s;
}

int LuaRun(lua_State * L, const char * filename){
    int s;

    // execute Lua program

    luaL_loadfile(L, filename);
    lua_State * L1 = lua_newthread(L);
    s = lua_resume(L1, L, 0);

    if(s!=LUA_OK && s!=LUA_YIELD){
        cout << "=================================================" << endl;
        cout << "Fatal lua error (" << s <<"): " << lua_tostring(L, -1) << endl;
        luaL_traceback(L, L, NULL, 0);
        cout << lua_tostring(L, -1) << endl;
        cout << "=================================================" << endl;
    }

    return s;
}

int tracebackf(lua_State * L){

    if (!lua_isstring(L, 1))  /* 'message' not a string? */
        return 1;  /* keep it intact */
    lua_getglobal(L, "debug");
    if (!lua_istable(L, -1)) {
        lua_pop(L, 1);
        return 1;
    }
    lua_getfield(L, -1, "traceback");
    if (!lua_isfunction(L, -1)) {
        lua_pop(L, 2);
        return 1;
    }
    lua_pushvalue(L, 1);  /* pass error message */
    lua_pushinteger(L, 2);  /* skip this function and traceback */
    lua_call(L, 2, 1);  /* call debug.traceback */

    //cout << "??!"

    cout << "=================================================" << endl;
    cout << "Fatal lua error: " << lua_tostring(L, -1) << endl;
    //luaL_traceback(L, L, NULL, 0);
    cout << "=================================================" << endl;

    return 1;
}

int LuaRun2(lua_State * L, const char * filename){
    int s, elements = lua_gettop(L);

    // execute Lua program
    lua_pushcfunction(L, &tracebackf);
    luaL_loadfile(L, filename);
    s = lua_pcall(L, 0, LUA_MULTRET, -2);

    lua_pop(L, lua_gettop(L)-elements);

    return s;
}

int LuaRun2(lua_State * L){
    int s, elements = lua_gettop(L);

    // execute Lua program
    lua_pushcfunction(L, &tracebackf);
    lua_pushvalue(L, -2);
    s = lua_pcall(L, 0, LUA_MULTRET, -2);

    lua_pop(L, lua_gettop(L)-elements+1);

    return s;
}
