///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "B_Initializer.h"

using namespace std;

btBroadphaseInterface * BulletPhysics::broadphase;
btDefaultCollisionConfiguration * BulletPhysics::collisionConfiguration;
btCollisionDispatcher * BulletPhysics::dispatcher;
btSequentialImpulseConstraintSolver * BulletPhysics::solver;
btDiscreteDynamicsWorld * BulletPhysics::dynamicsWorld;

void initializeBullet(){
    cout << "Initializing bullet!" << endl;

    // Build the broadphase
    BulletPhysics::broadphase = new btDbvtBroadphase();

    // Set up the collision configuration and dispatcher
    BulletPhysics::collisionConfiguration = new btDefaultCollisionConfiguration();
    BulletPhysics::dispatcher = new btCollisionDispatcher(BulletPhysics::collisionConfiguration);

    // The actual physics solver
    BulletPhysics::solver = new btSequentialImpulseConstraintSolver;

    // The world.
    BulletPhysics::dynamicsWorld = new btDiscreteDynamicsWorld(
        BulletPhysics::dispatcher,
        BulletPhysics::broadphase,
        BulletPhysics::solver,
        BulletPhysics::collisionConfiguration
        );
    BulletPhysics::dynamicsWorld->setGravity(btVector3(0, -10, 0));

    btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
    btDefaultMotionState* groundMotionState = new btDefaultMotionState(
        btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -50, 0)));
    btRigidBody::btRigidBodyConstructionInfo
            groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
    btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
    BulletPhysics::dynamicsWorld->addRigidBody(groundRigidBody);

    BulletPhysics::dynamicsWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

    // Do_everything_else_here


    return;
}

void finalizeBullet(){
    delete BulletPhysics::dynamicsWorld;
    delete BulletPhysics::solver;
    delete BulletPhysics::dispatcher;
    delete BulletPhysics::collisionConfiguration;
    delete BulletPhysics::broadphase;
}
