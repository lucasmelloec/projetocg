///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Light.h"

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Light::className[] = "Light";

Luna<L_Light>::RegType L_Light::Register[] = {
	method(L_Light, L_SetDiffuseColor, "SetDiffuseColor"),
	method(L_Light, L_SetSpecularColor, "SetSpecularColor"),
	method(L_Light, L_SetLightType, "SetLightType"),
	method(L_Light, L_SetRadius, "SetRadius"),
	method(L_Light, L_SetPosition, "SetPosition"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Light::L_Light() : L_Object(){

}

L_Light::~L_Light(){
	light->remove();
	lightRoot->remove();
}

// Lua side constructors

L_Light::L_Light(lua_State * L) : L_Object(L){
	lightRoot = irrmanager->addEmptySceneNode();
	light = irrmanager->addLightSceneNode(0, core::vector3df(0,0,1),
        video::SColorf(1.0f, 0.6f, 0.7f, 1.0f), 800.0f);
	light->setParent(lightRoot);
	light->getLightData().Type = video::ELT_DIRECTIONAL;
}

// Post-constructor / Pre-destructor

void L_Light::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Light::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);
}

// Lua binded methods

int L_Light::L_SetDiffuseColor(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Light:SetDiffuseColor() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Light:SetDiffuseColor() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float r = lua_tonumber(L, 2);
	float g = lua_tonumber(L, 3);
	float b = lua_tonumber(L, 4);

	light->getLightData().DiffuseColor = video::SColorf(r, g, b);

	return 0;
}

int L_Light::L_SetSpecularColor(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Light:SetSpecularColor() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Light:SetSpecularColor() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float r = lua_tonumber(L, 2);
	float g = lua_tonumber(L, 3);
	float b = lua_tonumber(L, 4);

	light->getLightData().DiffuseColor = video::SColorf(r, g, b);

	return 0;
}

int L_Light::L_SetLightType(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2){
			luaL_error(L, "Light:SetLightType() takes exactly one enum (Enum.LightType).");
			return 0;
		}else{
			if(!lua_isnumber(L, 2)){
				luaL_error(L, "Light:SetLightType() takes exactly one enum (Enum.LightType).");
				return 0;
			}
		}
	#endif

	lightType = lua_tonumber(L, 2);
	light->getLightData().Type = (video::E_LIGHT_TYPE)lightType;

	lightRoot->setRotation(core::vector3df(0, 0, 0));
	lightRoot->setPosition(core::vector3df(0, 0, 0));

	return 0;

}

int L_Light::L_SetRadius(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2){
			luaL_error(L, "Light:L_SetRadius() takes exactly one positive number.");
			return 0;
		}else{
			if(!lua_isnumber(L, 2) || lua_tonumber(L, 2) < 0){
				luaL_error(L, "Light:L_SetRadius() takes exactly one positive number.");
				return 0;
			}
		}
	#endif

	float radius = lua_tonumber(L, 2);
	light->setRadius(radius);

	return 0;
}

int L_Light::L_SetPosition(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Light:SetPosition() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Light:SetPosition() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	if((video::E_LIGHT_TYPE)lightType == video::ELT_DIRECTIONAL){
		float magnitude = x*x+y*y+z*z;
		magnitude = 1/sqrt(magnitude);

		x*=magnitude;
		y*=magnitude;
		z*=magnitude;

		core::CMatrix4<f32> mat;

		//lightRoot->setRotation(core::vector3df(x, y, z));
		lightRoot->setRotation(mat.buildRotateFromTo(
			core::vector3df(0, 0, -1),
			core::vector3df(x, y, z)).getRotationDegrees()
		);
		//light->updateAbsolutePosition();
	}else{
		lightRoot->setRotation(core::vector3df(0, 0, 0));
		lightRoot->setPosition(core::vector3df(x, y, z));
	}
	return 0;
}
