///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "ResumeQueue.h"

#include <iostream>

// TEMPORÁRIO
void report_errors(lua_State *L, int status)
{
  if ( status!=0 ) {
    std::cerr << "-- " << lua_tostring(L, -1) << std::endl;
    lua_pop(L, 1); // remove error message
  }
}

ResumeWait::ResumeWait(){

}

void ResumeWait::ResumeAll(lua_State * L){
	while(!toResume.empty() && toResume.top().first < globalTimer.elapsed()){
		//lua_State * current = toResume.top().second;
		//toResume.pop();

		//report_errors(L, lua_resume(L, current, 0));
		//cout << "Uhm" << endl;

		//lua_resume(current, L, 0);

		//cout << "Uhm2" << endl;
		//lua_resume(L, current, 0);
	}
}

ResumeEvent::ResumeEvent(){

}

void ResumeEvent::ResumeAll(lua_State * L){
	while(!toResume.empty()){
		lua_State * current = toResume.front();
		toResume.pop();

		lua_resume(current, L, 0);
	}
}

ResumeWait WaitQueue;
