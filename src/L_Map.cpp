///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Map.h"

#include "tiny_obj_loader.h"

#define max(a, b) (a) > (b) ? (a) : (b)
#define min(a, b) (a) > (b) ? (b) : (a)

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Map::className[] = "Map";

Luna<L_Map>::RegType L_Map::Register[] = {
	method(L_Map, L_AddModel, "AddModel"),
	method(L_Map, L_AddModelSegmented, "SegmentModel"),
	method(L_Map, L_AddOBJ, "AddOBJ"),
	method(L_Map, L_Build, "Build"),
	{0,0}
};

//== AUXILIARY FUNCTIONS / CLASSES =============================

		// GET FACE CENTER

vector<float> getFaceCenter(vector<tinyobj::shape_t> & obj, pair<int, int> face){
	int shapeId = face.first;
	int faceId = face.second;

	vector<float> ret;

	tinyobj::shape_t& shape = obj[shapeId];
	for(int i=0; i<3; i++){
		ret.push_back(
			(shape.mesh.positions[shape.mesh.indices[faceId+0]*3+i] +
			 shape.mesh.positions[shape.mesh.indices[faceId+1]*3+i] +
			 shape.mesh.positions[shape.mesh.indices[faceId+2]*3+i]) / 3.0
		);
	}

	ret[0] = -ret[0];

	return ret;
}



class util{
	public:
	vector<scene::SMeshBuffer *> * meshBuffers;
	vector<int> vertexCount;
	map<int, int> * toMeshbuffer;

	util(
		vector<scene::SMeshBuffer *> * a,
		map<int, int> * b
	){
		this->meshBuffers = a;
		this->toMeshbuffer = b;
	}

	int vertexIndex(vector<tinyobj::shape_t> & obj, int shape, int index){
		int vertexId = 0;

		if(shape >= (int)vertexCount.size()){
			vertexCount.resize(shape+1, 0);
		}

		vertexId = vertexCount[shape];
		vertexCount[shape]++;

		int realShape = (*toMeshbuffer)[shape];

		video::S3DVertex v;
		scene::SMeshBuffer * vMaterial = (*meshBuffers)[realShape];

		v.Color = vMaterial->Material.DiffuseColor;
		v.Pos = core::vector3df(
			-obj[shape].mesh.positions[index+0],
			obj[shape].mesh.positions[index+1],
			obj[shape].mesh.positions[index+2]
			);


		// irrmanager->addCubeSceneNode(0.5f, 0, -1, core::vector3df(
		// 	-obj[shape].mesh.normals[index+0]-obj[shape].mesh.positions[index+0],
		// 	obj[shape].mesh.normals[index+1]+obj[shape].mesh.positions[index+1],
		// 	obj[shape].mesh.normals[index+2]+obj[shape].mesh.positions[index+2]
		// 	));
		v.TCoords.set(0.0f,0.0f);
		v.Normal.set(
			-obj[shape].mesh.normals[index+0],
			obj[shape].mesh.normals[index+1],
			obj[shape].mesh.normals[index+2]
			);

		vMaterial->Vertices.push_back(v);

		return vertexId;
	}
};

	// RESCALE .OBJ

void rescaleShapeT(vector<tinyobj::shape_t> & obj, float scale){
	int i, j;

	for(i=0; i<(int)obj.size(); i++){
		for(j=0; j<(int)obj[i].mesh.positions.size(); j++){
			obj[i].mesh.positions[j] *= scale;
		}
	}
}

	// OCTREE NODE

const int maxPolys = (1<<16)/3;

class octreeNode{
		public:

		octreeNode ** children;
		bool isLeaf;

		vector<pair<int, int> > faces;
		int faceCount;

		float xBound[2] = {0, 0};
		float yBound[2] = {0, 0};
		float zBound[2] = {0, 0};
		float center[3];

		octreeNode(){
			children = NULL;

			isLeaf = true;
			faceCount = 0;
		}

		void calculateCenter(){
			center[0] = (xBound[0] + xBound[1])/2.0f;
			center[1] = (yBound[0] + yBound[1])/2.0f;
			center[2] = (zBound[0] + zBound[1])/2.0f;
		}

		octreeNode * getChild(int x, int y, int z){
			return children[(x<<0)+(y<<1)+(z<<2)];
		}

		vector<scene::IAnimatedMesh *> getMeshes(vector<tinyobj::shape_t> & obj){
			if(isLeaf){
				int i;

				vector<scene::SMeshBuffer *> meshBuffers;
				map<int, int> toMeshbuffer;

				util ut(
					&meshBuffers,
					&toMeshbuffer);

				for(i=0; i<(int)faces.size(); i++){
					scene::SMeshBuffer * faceMaterial = NULL;

					if(toMeshbuffer.find(faces[i].first)==toMeshbuffer.end()){
						// cout << "WTF!!" << endl;
						toMeshbuffer.insert(
							make_pair(
								faces[i].first,
								meshBuffers.size()
							)
						);

						scene::SMeshBuffer * newMaterial = new scene::SMeshBuffer();

						// wavefront shininess is from [0, 1000], so scale for OpenGL
									// shininessValue *= 0.128f;
									// currMaterial->Meshbuffer->Material.Shininess = shininessValue;

						newMaterial->Material.Shininess = obj[faces[i].first].material.shininess * 0.128f;
						newMaterial->Material.AmbientColor = video::SColorf(
							obj[faces[i].first].material.ambient[0],
							obj[faces[i].first].material.ambient[1],
							obj[faces[i].first].material.ambient[2],
							1.0f).toSColor();
						newMaterial->Material.DiffuseColor = video::SColorf(
							obj[faces[i].first].material.diffuse[0],
							obj[faces[i].first].material.diffuse[1],
							obj[faces[i].first].material.diffuse[2],
							1.0f).toSColor();
						// newMaterial->Material.SpecularColor = video::SColorf(
						// 	obj[faces[i].first].material.specular[0],
						// 	obj[faces[i].first].material.specular[1],
						// 	obj[faces[i].first].material.specular[2],
						// 	1.0f).toSColor();
						newMaterial->Material.SpecularColor = video::SColorf(
							0, 0, 0,
							1.0f).toSColor();
						newMaterial->Material.EmissiveColor = video::SColorf(
							obj[faces[i].first].material.emission[0],
							obj[faces[i].first].material.emission[1],
							obj[faces[i].first].material.emission[2],
							1.0f).toSColor();

						meshBuffers.push_back(newMaterial);
						faceMaterial = newMaterial;
					}else{
						faceMaterial = meshBuffers[toMeshbuffer[faces[i].first]];
					}

					//int materialId = toMeshbuffer[faces[i].first];

					faceMaterial->Indices.push_back(
						ut.vertexIndex(
								obj,
								faces[i].first,
								obj[faces[i].first].mesh.indices[faces[i].second+2]*3
							)
						);
					faceMaterial->Indices.push_back(
						ut.vertexIndex(
								obj,
								faces[i].first,
								obj[faces[i].first].mesh.indices[faces[i].second+1]*3
							)
						);
					faceMaterial->Indices.push_back(
						ut.vertexIndex(
								obj,
								faces[i].first,
								obj[faces[i].first].mesh.indices[faces[i].second+0]*3
							)
						);
				}

				scene::SMesh* mesh = new scene::SMesh();

				for(i=0; i<(int)meshBuffers.size(); i++){
					if(meshBuffers[i]->getIndexCount() > 0){
						meshBuffers[i]->recalculateBoundingBox();
						//irrmanager->getMeshManipulator()->recalculateNormals(meshBuffers[i]);
						mesh->addMeshBuffer( meshBuffers[i] );
					}
				}

				// Create the Animated mesh if there's anything in the mesh
				scene::SAnimatedMesh* animMesh = 0;
				if ( 0 != mesh->getMeshBufferCount() )
				{
					mesh->recalculateBoundingBox();
					animMesh = new scene::SAnimatedMesh();
					animMesh->Type = scene::EAMT_OBJ;
					animMesh->addMesh(mesh);
					animMesh->recalculateBoundingBox();
				}

				vector<scene::IAnimatedMesh *> ret;
				ret.push_back(animMesh);

				mesh->drop();

				return ret;
			}else{
				vector<scene::IAnimatedMesh *> ret;

				for(int i=0; i<8; i++){
					vector<scene::IAnimatedMesh *> tmp = children[i]->getMeshes(obj);

					for(int j=0; j<(int)tmp.size(); j++){
						if(tmp[j]!=NULL){
							ret.push_back(tmp[j]);
						}
					}
				}

				return ret;
			}
		}

		void addFace(vector<tinyobj::shape_t> & obj, pair<int, int> face){
			int i, j;

			if(isLeaf){
				if(faceCount + 1 >= maxPolys){
					isLeaf = false;

					children = (octreeNode **)malloc(8*sizeof(octreeNode *));
					for(i=0; i<8; i++){
						children[i] = new octreeNode();

						children[i]->xBound[0] =
							((i>>0) & 1) ? center[0] : xBound[0];
						children[i]->xBound[1] =
							((i>>0) & 1) ? xBound[1] : center[0];

						children[i]->yBound[0] =
							((i>>1) & 1) ? center[1] : yBound[0];
						children[i]->yBound[1] =
							((i>>1) & 1) ? yBound[1] : center[1];

						children[i]->zBound[0] =
							((i>>2) & 1) ? center[2] : zBound[0];
						children[i]->zBound[1] =
							((i>>2) & 1) ? zBound[1] : center[2];

						children[i]->calculateCenter();
					}

					faceCount = 0;

					for(j=0; j<(int)faces.size(); j++){
						addFace(obj, faces[j]);
					}

					addFace(obj, face);
					return;
				}else{
					faceCount++;
					faces.push_back(face);
					return;
				}
			}else{
				faceCount++;
				vector<float> faceCenter = getFaceCenter(obj, face);

				getChild(
					faceCenter[0] > center[0] ? 1 : 0,
					faceCenter[1] > center[1] ? 1 : 0,
					faceCenter[2] > center[2] ? 1 : 0
				)->addFace(obj, face);
				return;
			}
		}
	};

//== EXPOSED CLASS FUNCTIONS ===================================

// C++ side constructors

L_Map::L_Map() : L_Object(){

}

L_Map::~L_Map(){
	// light->remove();
	// lightRoot->remove();
}

// Lua side constructors

L_Map::L_Map(lua_State * L) : L_Object(L){
	built = false;
	rigidBody = NULL;
	Changed = NULL;
}

// Post-constructor / Pre-destructor

void L_Map::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Map::DestroyObject(lua_State * L){
	if(Changed!=NULL)
		Changed->Destroy(L);

	if(rigidBody!=NULL){
		BulletPhysics::dynamicsWorld->removeRigidBody(rigidBody);

		delete rigidBody->getMotionState();
		delete rigidBody;
	}

	unsigned int i;

	for(i=0; i<sceneNodes.size(); i++){
		sceneNodes[i]->remove();
	}
}

// Lua binded methods

int L_Map::L_AddModelSegmented(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=3&&lua_gettop(L)!=4&&lua_gettop(L)!=7&&lua_gettop(L)!=11){
			luaL_error(L, "Map:AddModel() needs at least a filename and whether or not is should be rendered using an octree.");
		}
	#endif

    //scene::IAnimatedMesh* mesh = irrmanager->getMesh(lua_tostring(L, 2));

	// First we get the Lua parameters

    bool isOctree = lua_toboolean(L, 3);
    float scale = 1;

	core::vector3df origin;
	btQuaternion rotation;
	rotation.setEuler(0, 0, 0);

	if(lua_gettop(L)>=4){
    	scale = lua_tonumber(L, 4);

		if(lua_gettop(L)>=7){
			origin = core::vector3df(
				lua_tonumber(L, 5),
				lua_tonumber(L, 6),
				lua_tonumber(L, 7)
				);

			if(lua_gettop(L)==11){
				rotation = btQuaternion(
					lua_tonumber(L, 8),
					lua_tonumber(L, 9),
					lua_tonumber(L, 10),
					lua_tonumber(L, 11)
					);
			}
		}
	}

	// Next, we read the .OBJ file

	vector<tinyobj::shape_t> obj;
	tinyobj::LoadObj(
		obj,
		lua_tostring(L, 2)
		);

	rescaleShapeT(obj, scale);

	// Then we set up the octree for segmentation

	int i;

	octreeNode * root = new octreeNode();

	// Calculating model boundaries to supply the octree root with data

	for (i=0;i<(int)obj.size();i++)
	{
		tinyobj::shape_t& shape = obj[i];
		int faceCount = shape.mesh.indices.size();

		for(int face=0; face<faceCount; face+=3){
			vector<float> faceCenter = getFaceCenter(obj, make_pair(i, face));

			root->xBound[0] = min(
				root->xBound[0],
				faceCenter[0]
				);
			root->xBound[1] = max(
				root->xBound[1],
				faceCenter[0]
				);
			root->yBound[0] = min(
				root->yBound[0],
				faceCenter[1]
				);
			root->yBound[1] = max(
				root->yBound[1],
				faceCenter[1]
				);
			root->zBound[0] = min(
				root->zBound[0],
				faceCenter[2]
				);
			root->zBound[1] = max(
				root->zBound[1],
				faceCenter[2]
				);

		}
	}

	root->calculateCenter();

	// Inserting triangles into segment tree

	for (i=0;i<(int)obj.size();i++)
	{
		tinyobj::shape_t& shape = obj[i];
		int faceCount = shape.mesh.indices.size();

		for(int face=0; face<faceCount; face+=3){
			root->addFace(obj, make_pair(i, face));
		}
	}


	// Building meshes

	vector<scene::IAnimatedMesh *> meshes = root->getMeshes(obj);
	cout << meshes.size() << " meshes" << endl;
	cout << root->faceCount << " triangles" << endl;
	cout << obj.size() << " shapes" << endl;

	for(i=0; i<(int)meshes.size(); i++){
		scene::ISceneNode * currentNode = NULL;

		if(isOctree)
			currentNode = irrmanager->addOctreeSceneNode(meshes[i]->getMesh(0), 0, -1, 1024);
		else
			currentNode = irrmanager->addMeshSceneNode(meshes[i]->getMesh(0));

		btVector3 euler;

		QuaternionToEuler(rotation, euler);

		//currentNode->setRotation(core::vector3df(euler.x(), euler.y(), euler.z()));
		currentNode->setPosition(origin);

		//currentNode->setMaterialFlag(video::EMF_BACK_FACE_CULLING, false);
	    //currentNode->setMaterialFlag(video::EMF_LIGHTING, true);
	    //currentNode->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);
	    //currentNode->setMaterialFlag(video::EMF_COLOR_MATERIAL, true);

		sceneNodes.push_back(currentNode);
	}

	return 0;
}

int L_Map::L_AddModel(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=3&&lua_gettop(L)!=6&&lua_gettop(L)!=10){
			luaL_error(L, "Map:AddModel() needs at least a filename and whether or not is should be rendered using an octree.");
		}
	#endif

    scene::IAnimatedMesh* mesh = irrmanager->getMesh(lua_tostring(L, 2));
    bool isOctree = lua_toboolean(L, 3);

	if(!mesh){
		luaL_error(L, "Invalid string");
	}else{
		core::vector3df origin;
		btQuaternion rotation;
		rotation.setEuler(0, 0, 0);

		if(lua_gettop(L)>=6){
			origin = core::vector3df(
				lua_tonumber(L, 4),
				lua_tonumber(L, 5),
				lua_tonumber(L, 6)
				);

			if(lua_gettop(L)==10){
				rotation = btQuaternion(
					lua_tonumber(L, 7),
					lua_tonumber(L, 8),
					lua_tonumber(L, 9),
					lua_tonumber(L, 10)
					);
			}
		}

		scene::ISceneNode * currentNode = NULL;

		if(isOctree)
			currentNode = irrmanager->addOctreeSceneNode(mesh->getMesh(0), 0, -1, 1024);
		else
			currentNode = irrmanager->addMeshSceneNode(mesh->getMesh(0));

		btVector3 euler;

		QuaternionToEuler(rotation, euler);

		currentNode->setRotation(core::vector3df(euler.x(), euler.y(), euler.z()));
		currentNode->setPosition(origin);

		sceneNodes.push_back(currentNode);
	}

	return 0;
}

int L_Map::L_AddOBJ(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2&&lua_gettop(L)!=3&&lua_gettop(L)!=6&&lua_gettop(L)!=10){
			luaL_error(L, "Map:AddOBJ() needs at least a filename.");
		}
	#endif

	btVector3 origin;
	btQuaternion rotation;
	rotation.setEuler(0, 0, 0);

	float scale = 1;

	if(lua_gettop(L)>=3){
		scale = lua_tonumber(L, 3);

		if(lua_gettop(L)>=6){
			origin = btVector3(
				lua_tonumber(L, 4),
				lua_tonumber(L, 5),
				lua_tonumber(L, 6)
				);

			if(lua_gettop(L)==10){
				rotation = btQuaternion(
					lua_tonumber(L, 7),
					lua_tonumber(L, 8),
					lua_tonumber(L, 9),
					lua_tonumber(L, 10)
					);
			}
		}
	}

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(origin);
    transform.setRotation(rotation);

	vector<tinyobj::shape_t> obj;
	tinyobj::LoadObj(
		obj,
		lua_tostring(L, 2)
		);

	rescaleShapeT(obj,scale);

	for (int s=0;s<(int)obj.size();s++)
	{
		tinyobj::shape_t& shape = obj[s];
		int faceCount = shape.mesh.indices.size();

		for(int face=0; face<faceCount; face+=3){
			Triangle t = {
				transform*btVector3(
					-shape.mesh.positions[shape.mesh.indices[face]*3+0],
					shape.mesh.positions[shape.mesh.indices[face]*3+1],
					shape.mesh.positions[shape.mesh.indices[face]*3+2]
				),
				transform*btVector3(
					-shape.mesh.positions[shape.mesh.indices[face+1]*3+0],
					shape.mesh.positions[shape.mesh.indices[face+1]*3+1],
					shape.mesh.positions[shape.mesh.indices[face+1]*3+2]
				),
				transform*btVector3(
					-shape.mesh.positions[shape.mesh.indices[face+2]*3+0],
					shape.mesh.positions[shape.mesh.indices[face+2]*3+1],
					shape.mesh.positions[shape.mesh.indices[face+2]*3+2]
				)
			};

			triangles.push_back(t);
		}
	}

	return 0;
}

int L_Map::L_Build(lua_State *L){
	if(built){
		luaL_error(L, "Map was already built.");
		return 0;
	}
	built = true;

	btTriangleMesh * btMesh = new btTriangleMesh();

	bool removeDuplicateVertices = false;
	for(unsigned int i=0; i<triangles.size(); i++){
		btMesh->addTriangle(
			triangles[i].vx1,
			triangles[i].vx2,
			triangles[i].vx3,
			removeDuplicateVertices
			);
	}

    // Create the shape
    btCollisionShape *btShape = new btBvhTriangleMeshShape( btMesh, true );
    btShape->setMargin( 0.05f );

	 // Give it a default MotionState
    btTransform transform;
    transform.setIdentity();

    //btDefaultMotionState *motionState = new btDefaultMotionState( transform );
	gameMotionState * motionState = new gameMotionState(transform);

	btVector3 LocalInertia;
	btShape->calculateLocalInertia(1, LocalInertia);

    // Create the rigid body object
    btScalar mass = 0.0f;

    rigidBody = new btRigidBody( mass, motionState, btShape, LocalInertia );
	BulletPhysics::dynamicsWorld->addRigidBody(rigidBody, COL_DEFAULT, COL_DEFAULT);

	return 0;
}
