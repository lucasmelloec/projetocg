///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Model.h"

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Model::className[] = "Model";

Luna<L_Model>::RegType L_Model::Register[] = {
	//method(L_Model, L_SetDiffuseColor, "SetDiffuseColor"),
	//method(L_Model, L_SetSpecularColor, "SetSpecularColor"),
	method(L_Model, L_SetPosition, "SetPosition"),
	method(L_Model, L_SetRotation, "SetRotation"),
	method(L_Model, L_SetSize, "SetSize"),
	method(L_Model, L_GetPosition, "GetPosition"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Model::L_Model() : L_Object(){
	sceneNode = NULL;
	motionState = NULL;
}

L_Model::~L_Model(){

}

// Lua side constructors

L_Model::L_Model(lua_State * L) : L_Object(L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)==0){
			lua_pushstring(L, "Model constructor needs a filename.");
			lua_error(L);
		}
	#endif

    scene::IAnimatedMesh* mesh = irrmanager->getMesh(lua_tostring(L, 1));

	if(!mesh){
		Changed = NULL;

		luaL_error(L, "Invalid string");
	}else{
		sceneNode = irrmanager->addMeshSceneNode(mesh->getMesh(0));

		((scene::IMeshSceneNode *)sceneNode)->addShadowVolumeSceneNode();
		irrmanager->setShadowColor(video::SColor(150,0,0,0));
	}

	motionState = NULL;
}

// Post-constructor / Pre-destructor

void L_Model::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
}

void L_Model::DestroyObject(lua_State * L){
	if(motionState != NULL) {
		for(vector<pair<scene::ISceneNode *, btTransform> >::iterator it = motionState->sceneNodes.begin(); it != motionState->sceneNodes.end(); it++) {
			if((*it).first == sceneNode) {
				motionState->sceneNodes.erase(it);
				break;
			}
		}
	}

	if(sceneNode!=NULL){
		sceneNode->remove();
		sceneNode = NULL;
	}

	if(Changed!=NULL){
		Changed->Destroy(L);
		Changed = NULL;
	}
}

// Lua binded methods

/*int L_Model::L_SetDiffuseColor(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Box:SetDiffuseColor() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Box:SetDiffuseColor() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float r = lua_tonumber(L, 2);
	float g = lua_tonumber(L, 3);
	float b = lua_tonumber(L, 4);

	cout << r << g << b << endl;

	return 0;
}

int L_Model::L_SetSpecularColor(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Box:SetSpecularColor() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Box:SetSpecularColor() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float r = lua_tonumber(L, 2);
	float g = lua_tonumber(L, 3);
	float b = lua_tonumber(L, 4);

	cout << r << g << b << endl;

	return 0;
}*/

int L_Model::L_SetPosition(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Model:SetPosition() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Model:SetPosition() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	sceneNode->setPosition(core::vector3df(x, y, z));

	return 0;
}

int L_Model::L_SetRotation(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Model:SetRotation() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Model:SetRotation() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	sceneNode->setPosition(core::vector3df(x, y, z));

	return 0;
}

int L_Model::L_SetSize(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Model:SetSize() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Model:SetSize() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	sceneNode->setScale(core::vector3df(x, y, z));

	return 0;
}

int L_Model::L_GetPosition(lua_State *L){
#if LUA_CHECK_ERRORS
	if(lua_gettop(L)!=1){
			lua_pushstring(L, "Model:GetPosition() accepts no argument.");
			lua_error(L);

			return 0;
		}
#endif

    core::vector3df pos = sceneNode->getAbsolutePosition();
	lua_pushnumber(L, pos.X);
	lua_pushnumber(L, pos.Y);
	lua_pushnumber(L, pos.Z);

	return 3;
}
