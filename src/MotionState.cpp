///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "MotionState.h"

gameMotionState::gameMotionState(const btTransform &initialPosition)
{
    mInitialPosition = initialPosition;
}

gameMotionState::~gameMotionState()
{
}

void gameMotionState::getWorldTransform(btTransform &worldTrans) const
{
    worldTrans = mInitialPosition;
}

void gameMotionState::setWorldTransform(const btTransform &worldTrans)
{
    //btQuaternion rot = worldTrans.getRotation();
    //mSceneNode ->setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
    //mSceneNode ->setPosition(pos.x(), pos.y(), pos.z());

    for(unsigned int i=0; i<sceneNodes.size(); i++){
        btTransform objectTrans;
        objectTrans = worldTrans * sceneNodes[i].second;
        // objectTrans.mult(sceneNodes[i].second, worldTrans);

        btVector3 pos = objectTrans.getOrigin();
        sceneNodes[i].first->setPosition(core::vector3df(pos.x(), pos.y(), pos.z()));

        btVector3 er = btVector3(); // Euler Rotation
        QuaternionToEuler(objectTrans.getRotation(), er);
        sceneNodes[i].first->setRotation(core::vector3df(er.x(), er.y(), er.z()));
    }

    return;
}

void gameMotionState::setKinematicPosition(const btTransform & worldTrans){
	mInitialPosition = worldTrans;

    setWorldTransform(worldTrans);
}

void gameMotionState::addSceneNode(scene::ISceneNode * node, btTransform transf){
    sceneNodes.push_back(make_pair(node, transf));

    return;
}

void gameMotionState::addSceneNode(scene::ISceneNode * node){
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(0, 0, 0));

    sceneNodes.push_back(make_pair(node, transform));

    return;
}

void gameMotionState::removeSceneNode(scene::ISceneNode * node){
    unsigned int i;

    for(i=0; i<sceneNodes.size(); i++){
        if(sceneNodes[i].first==node){
            sceneNodes.erase(sceneNodes.begin()+i);
            break;
        }
    }

    return;
}

void QuaternionToEuler(const btQuaternion &TQuat, btVector3 &TEuler) {
	btScalar W = TQuat.getW();
	btScalar X = TQuat.getX();
	btScalar Y = TQuat.getY();
	btScalar Z = TQuat.getZ();
	float WSquared = W * W;
	float XSquared = X * X;
	float YSquared = Y * Y;
	float ZSquared = Z * Z;

	TEuler.setX(atan2f(2.0f * (Y * Z + X * W), -XSquared - YSquared + ZSquared + WSquared));
	TEuler.setY(asinf(-2.0f * (X * Z - Y * W)));
	TEuler.setZ(atan2f(2.0f * (X * Y + Z * W), XSquared - YSquared - ZSquared + WSquared));
	TEuler *= core::RADTODEG;
}
