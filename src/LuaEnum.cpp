///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "LuaEnum.h"
#include "Globals.h"

#include <iostream>

using namespace std;

struct EnumNamespace{
	const char * name;
	const bool isNamespace;
	const int value;
};

const EnumNamespace enumList[] = {
	{"LightType", true, 0},
		{"Directional", false, video::ELT_DIRECTIONAL},
		{"Point", false, video::ELT_POINT},
		{"Spot", false, video::ELT_SPOT},
	{"Keys", true, 0},
		{"w", false, irr::KEY_KEY_W},
		{"a", false, irr::KEY_KEY_A},
		{"s", false, irr::KEY_KEY_S},
		{"d", false, irr::KEY_KEY_D},
		{"UP", false, irr::KEY_UP},
		{"LEFT", false, irr::KEY_LEFT},
		{"DOWN", false, irr::KEY_DOWN},
		{"RIGHT", false, irr::KEY_RIGHT},
        {"F1", false, irr::KEY_F1},
	{"Collision", true, 0},
		{"Nothing", false, COL_NOTHING},
		{"Default", false, COL_DEFAULT},
		{"NoCollide", false, COL_NOCOLLIDE},
	{"", true, 0}
};

void LuaSetEnums(lua_State * L){
	int i;

	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setglobal(L, "Enum");

	bool initialized = false;

	for(i=0; enumList[i].name[0]!='\0'; i++){
		if(enumList[i].isNamespace){
			if(initialized){
				// (Enum) (PastNamespace)
				lua_pop(L, 1);
			}else{
				initialized = true;
			}

			// (Enum)

			lua_newtable(L);
			lua_pushstring(L, enumList[i].name);
			lua_pushvalue(L, -2);

			// (Enum) (DesiredNamespace) (NamespaceName) (DesiredNamespace)

			lua_settable(L, -4);
		}else{
			// (Enum) (DesiredNamespace)

			lua_pushstring(L, enumList[i].name);
			lua_pushinteger(L, enumList[i].value);
			lua_settable(L, -3);
		}
	}

	lua_pop(L, 2);

	return;
}
