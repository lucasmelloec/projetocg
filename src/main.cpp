///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include <iostream>
#include <unistd.h>
#include <cmath>
#include <string>
#include <map>

#include "irrlicht/irrlicht.h"
#include "irrlicht/driverChoice.h"

#include "luah.h"
#include "luna.h"

#include "Globals.h"
#include "ResumeQueue.h"
#include "L_Object.h"
#include "L_Light.h"
#include "L_Convex.h"
#include "L_Map.h"
#include "L_Model.h"
#include "PerlinNoise.h"
#include "LuaEnum.h"
#include "InputHandler.h"
#include "Particles.h"
#include "L_Hinge.h"
#include "L_Camera.h"
#include "L_Pathfinding.h"
#include "L_Perlin.h"

#include "B_Initializer.h"

using namespace std;
using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

void InitializeLuna(lua_State * L){
	cout << "Registering classes in Lua" << endl;

	Luna<L_Object>::Register(L);
	Luna<L_Light>::Register(L);
	Luna<L_Map>::Register(L);
	Luna<L_Hinge>::Register(L);
	Luna<L_Convex>::Register(L);
	Luna<L_Model>::Register(L);
	Luna<L_Camera>::Register(L);
	Luna<L_Pathfinding>::Register(L);
	Luna<L_Perlin>::Register(L);

    Particles::initializeParticles(L);
}

int LuaTick(lua_State * L){
	lua_pushnumber(L, globalTimer.elapsed());

	return 1;
}

int LuaGetWindowSize(lua_State * L){
	lua_pushnumber(L, irrdriver->getScreenSize().Width);
	lua_pushnumber(L, irrdriver->getScreenSize().Height);

	return 2;
}

int LuaExplodeVolume(lua_State * L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=5){
			lua_pushstring(L, "ExplodeVolume() accepts five numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 1) || !lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4) || !lua_isnumber(L, 5)){
			lua_pushstring(L, "ExplodeVolume() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x, y, z, r, maxIntesity;
	x = lua_tonumber(L, 1);
	y = lua_tonumber(L, 2);
	z = lua_tonumber(L, 3);
	r = lua_tonumber(L, 4);
	maxIntesity = lua_tonumber(L, 5);

	std::vector<btRigidBody*> results;

	//[1]
	btVector3 explosionCenter(x, y, z);
	btSphereShape sphere(r);
	btPairCachingGhostObject ghost;
	btTransform xform;
	xform.setIdentity();
	xform.setOrigin(explosionCenter);
	ghost.setCollisionShape(&sphere);
	ghost.setWorldTransform(xform);

	//[2]
	BulletPhysics::dynamicsWorld->addCollisionObject(&ghost, btBroadphaseProxy::DefaultFilter, btBroadphaseProxy::AllFilter);

	//[3]
	for (int i = 0; i < ghost.getNumOverlappingObjects(); i++){
	    btRigidBody *btco = btRigidBody::upcast(ghost.getOverlappingObject(i));
			if(btco != NULL) {
				results.push_back(btco);
			}
	}

	//[4]
	BulletPhysics::dynamicsWorld->removeCollisionObject(&ghost);

	for(btRigidBody* body : results) {
		btVector3 distanceVector = body->getCenterOfMassTransform().getOrigin() - explosionCenter;

		float intensity = 0;

		if(distanceVector.length() < r) {
			// intensity = sqrt(explosionRadius^2 + distance^2)
			intensity = sqrt(r*r - distanceVector.length()*distanceVector.length()) * (maxIntesity/r);
		}

		body->applyCentralImpulse(distanceVector.normalize() * intensity);
	}

	results.clear();

	return 0;
}

int pushAny(lua_State * L){
	lua_pushstring(L, "Hello!");
	return 1;
}

lua_State * initializeLua(){
	objstoreCurrent = 0;

	lua_State * L = luaL_newstate();
    luaL_openlibs(L);

	InitializeLuna(L);

   	//lua_pushcfunction(L, &LuaWait);
    //lua_setglobal(L, "wait");

   	lua_pushcfunction(L, &LuaTick);
    lua_setglobal(L, "tick");

   	lua_pushcfunction(L, &LuaGetWindowSize);
    lua_setglobal(L, "GetWindowSize");

   	lua_pushcfunction(L, &LuaExplodeVolume);
    lua_setglobal(L, "ExplodeVolume");

    lua_newtable(L);
    lua_setglobal(L, "shared");

    lua_newtable(L);
    lua_setglobal(L, "glib");

    lua_newtable(L);
    lua_setglobal(L, "objstore");

    lua_pushcfunction(L, setObjectRoot);
    lua_setglobal(L, "setRoot");

    LuaSetEnums(L);

    LuaRun2(L, "./scripts/wait.lua");

    LuaRun2(L, "./scripts/glib.lua");

    inputHandler.initializeLua(L);

   	LuaRun2(L, "./scripts/ScriptManager.lua");

   	return L;
}

int main(){
	std::ios_base::sync_with_stdio(0);
	globalTimer = Timer();

	lua_State *L = initializeLua();
    initializeBullet();

    // Irrlicht initialization

	// video::E_DRIVER_TYPE driverType=driverChoiceConsole();
	// if (driverType==video::EDT_COUNT)
	// 	return 1;

    device = createDevice(EDT_OPENGL, dimension2d<u32>(1400, 900), 16, false, false, false, &inputHandler);
//    IrrlichtDevice * device = createDevice(driverType, dimension2d<u32>(800, 600), 16, true, true, false, &inputHandler);
    device->setWindowCaption(L"TankGame");

    irrdriver = device->getVideoDriver();
	irrmanager = device->getSceneManager();
	IGUIEnvironment * guienv = device->getGUIEnvironment();

	irrmanager->setAmbientLight(SColorf(0.05, 0.05, 0.05, 0));

    //irrdriver->getDriverAttributes().setAttribute("AntiAlias", 16);

	irrmanager->setShadowColor(video::SColor(0,0,0,0));

	irrmanager->addSkyDomeSceneNode(irrdriver->getTexture("./assets/textures/sky2.jpg"));

	//irrmanager->setAmbientLight(SColorf(0.0, 0.0, 0.0, 0.5));

	#if PROFILE_LOOP
		double graphicsAverage = 0;
	#endif

	std::map<core::vector3df, int> bmap;
	bmap[core::vector3df(3218.32131*321321.321, 3218.32131*321321.321, 3218.32131*321321.321)] = 3;

	cout << (bmap.find(core::vector3df(3218.32131*321321.321, 3218.32131*321321.321, 3218.32131*321321.321))==bmap.end()) << endl;

    double laststep = globalTimer.elapsed();

	while(device->run()){
		double currentstep = globalTimer.elapsed();
		double delta = currentstep - laststep;

		laststep = currentstep;

		#if PROFILE_LOOP
			Timer profiler;
		#endif

			// Physics step
		BulletPhysics::dynamicsWorld->stepSimulation(delta, 60);

		int numManifolds = BulletPhysics::dynamicsWorld->getDispatcher()->getNumManifolds();
		for(int i=0; i<numManifolds; i++) {
	    btPersistentManifold* pm = BulletPhysics::dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
	    if(pm->getNumContacts() > 0) {
	       btCollisionObject* test1 = const_cast<btCollisionObject*>(pm->getBody0());
	       btCollisionObject* test2 = const_cast<btCollisionObject*>(pm->getBody1());

				 btRigidBody *bodyA = btRigidBody::upcast(test1);
				 btRigidBody *bodyB = btRigidBody::upcast(test2);

				 std::map<btRigidBody*, L_Convex*>::iterator it = L_Convex::rigidMap.find(bodyA);
					 if(it != L_Convex::rigidMap.end()) {
					 		it->second->collided(L);
					 }
				 it = L_Convex::rigidMap.find(bodyB);
					 if(it != L_Convex::rigidMap.end()) {
					 		it->second->collided(L);
					 }
	    }
		}

		#if PROFILE_LOOP
			cout << "Physics: " << profiler.elapsed() << endl;
			profiler.reset();
		#endif

			// Graphics step

		irrdriver->beginScene(true, true, SColor(255,100,101,140));

		irrmanager->drawAll();
		guienv->drawAll();

		irrdriver->endScene();

		#if PROFILE_LOOP
			double t = profiler.elapsed();
			cout << "Graphics: " << profiler.elapsed() << endl;
			profiler.reset();

			graphicsAverage = graphicsAverage*(0.99)+t*(0.01);
		#endif

			// Input step (for Lua)

		inputHandler.handleEvents(L);

		#if PROFILE_LOOP
			cout << "Input: " << profiler.elapsed() << endl;
			profiler.reset();
		#endif

			// Lua step

    	lua_getglobal(L, "resumeWait");
    	LuaRun2(L);

		#if PROFILE_LOOP
			cout << "Lua: " << profiler.elapsed() << endl;
			profiler.reset();
		#endif


		Particles::update();

    	//usleep(1000);
	}

	inputHandler.Destroy(L);

    lua_close(L);

	device->drop();

	//delete guienv;
	//delete irrmanager;
	//delete irrdriver;
	//delete device;

	finalizeBullet();

	#if PROFILE_LOOP
		cout << "Graphics average delay: " << graphicsAverage << endl;
	#endif

	return 0;
}
