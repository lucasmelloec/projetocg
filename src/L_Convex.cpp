///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "L_Convex.h"

map<btRigidBody *, L_Convex *> L_Convex::rigidMap;

//== LUA REGISTER ==============================================

#define method(class, name, luaname) {luaname, &class::name}

const char L_Convex::className[] = "Convex";

Luna<L_Convex>::RegType L_Convex::Register[] = {
	//method(L_Convex, L_SetDiffuseColor, "SetDiffuseColor"),
	//method(L_Convex, L_SetSpecularColor, "SetSpecularColor"),
	method(L_Convex, L_SetPosition, "SetPosition"),
	method(L_Convex, L_SetRotation, "SetRotation"),
	method(L_Convex, L_SetSize, "SetSize"),
	method(L_Convex, L_SetMass, "SetMass"),
	method(L_Convex, L_AddModel, "AddModel"),
	method(L_Convex, L_ToWorld, "ToWorld"),
	method(L_Convex, L_ToObject, "ToObject"),
	method(L_Convex, L_SetVelocity, "SetVelocity"),
	method(L_Convex, L_SetAngularVelocity, "SetAngularVelocity"),
	method(L_Convex, L_GetPosition, "GetPosition"),
	method(L_Convex, L_ApplyImpulse, "ApplyImpulse"),
	method(L_Convex, L_SetGravity, "SetGravity"),
	{0,0}
};

//==============================================================

// C++ side constructors

L_Convex::L_Convex() : L_Object(){
	rigidBody = NULL;
}

L_Convex::~L_Convex(){

}

// Lua side constructors

L_Convex::L_Convex(lua_State * L) : L_Object(L){
	vector<tinyobj::shape_t> obj;
	tinyobj::LoadObj(
		obj,
		lua_tostring(L, 1)
		);

	//btTriangleMesh * btMesh = new btTriangleMesh();
	btConvexHullShape * btShape = new btConvexHullShape();

	for (int s=0;s<(int)obj.size();s++)
	{
		tinyobj::shape_t& shape = obj[s];
		int vertexCount = shape.mesh.positions.size();

		for(int ivx=0; ivx<vertexCount; ivx+=3){
			btShape->addPoint(btVector3(
				-shape.mesh.positions[ivx+0],
				shape.mesh.positions[ivx+1],
				shape.mesh.positions[ivx+2]
				), true);
		}
	}

    // Create the shape
    btShape->setMargin( 0.05f );

    btVector3 origin = btVector3(0, 0, 0);

	 // Give it a default MotionState
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(origin);
    //transform.setOrigin( position );
    //btDefaultMotionState *motionState = new btDefaultMotionState( transform );
	gameMotionState * motionState = new gameMotionState(transform);


	btVector3 LocalInertia;
	btShape->calculateLocalInertia(1, LocalInertia);

    // Create the rigid body object
    bodyMass = 1.0f;
    btScalar mass = bodyMass;

    btRigidBody::btRigidBodyConstructionInfo cinfo = btRigidBody::btRigidBodyConstructionInfo(mass, motionState, btShape, LocalInertia);
    if(lua_gettop(L)==2){


    	lua_pushstring(L, "friction");
    	lua_gettable(L, 2);

    	if(!lua_isnil(L, -1)){
    		cinfo.m_friction = lua_tonumber(L, -1);
    	}

    	lua_pop(L, 2);
    }

    rigidBody = new btRigidBody( cinfo );
    if(lua_gettop(L)==1){
		BulletPhysics::dynamicsWorld->addRigidBody(rigidBody, COL_DEFAULT, COL_DEFAULT);
    }else{
    	BulletPhysics::dynamicsWorld->addRigidBody(rigidBody, lua_tointeger(L, 2), lua_tointeger(L, 3));
    }

		L_Convex::rigidMap[rigidBody] = this;
}

// Post-constructor / Pre-destructor

void L_Convex::InitializeObject(lua_State *L){
	Changed = new LuaEvent(L, "Changed");
	collisionDetected = new LuaEvent(L, "CollisionDetected");
}

void L_Convex::DestroyObject(lua_State * L){
	if(rigidBody!=NULL){
		L_Convex::rigidMap.erase(rigidBody);

		BulletPhysics::dynamicsWorld->removeRigidBody(rigidBody);

		delete rigidBody->getMotionState();
		delete rigidBody;

		rigidBody = NULL;
	}

	if(collisionDetected != NULL) {
		collisionDetected->Destroy(L);
		collisionDetected = NULL;
	}

	if(Changed!=NULL){
		Changed->Destroy(L);
		Changed = NULL;
	}
}

int L_Convex::L_SetPosition(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetPosition() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetPosition() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	if(bodyMass==0.0f){
		btTransform transf;
		gameMotionState * m = (gameMotionState *)rigidBody->getMotionState();
		m->getWorldTransform(transf);
		transf.setOrigin(btVector3(x, y, z));
		m->setKinematicPosition(transf);
	}else{
		btTransform tr = rigidBody->getCenterOfMassTransform();
		tr.setOrigin(
			btVector3(x, y, z)
			);

		rigidBody->setCenterOfMassTransform(tr);

		// rigidBody->translate(
		// 	btVector3(
		// 	x, y, z
		// 	) - rigidBody->getCenterOfMassPosition());
	}

	for(int i=0; i<(int)connectedParts.size(); i++){
		connectedParts[i]->getRigidBodyB().setCenterOfMassTransform(
			rigidBody->getCenterOfMassTransform() * (connectedParts[i]->getFrameOffsetA() * connectedParts[i]->getFrameOffsetB().inverse())
		);
	}

	return 0;
}

int L_Convex::L_SetRotation(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=5&&lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetRotation() accepts four or three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetRotation() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	btTransform trans;
	trans.setIdentity();
	trans.setOrigin(btVector3(0, 0, 0));

	if(lua_gettop(L)==4){
		btQuaternion t;
		t.setEuler(
			lua_tonumber(L, 2),
			lua_tonumber(L, 3),
			lua_tonumber(L, 4)
		);

		trans.setRotation(t);
	}else{
		cout << lua_tonumber(L, 2) << " / " << lua_tonumber(L, 3) << " / " << lua_tonumber(L, 4) << " / " << lua_tonumber(L, 5) << endl;
 		trans.setRotation(btQuaternion(
					lua_tonumber(L, 2),
					lua_tonumber(L, 3),
					lua_tonumber(L, 4),
					lua_tonumber(L, 5)
					).normalize()
			);
	}

	// trans.setOrigin(rigidBody->getCenterOfMassPosition());
	rigidBody->setCenterOfMassTransform(rigidBody->getCenterOfMassTransform() * trans);

	for(int i=0; i<(int)connectedParts.size(); i++){
		connectedParts[i]->getRigidBodyB().setCenterOfMassTransform(
			rigidBody->getCenterOfMassTransform() * (connectedParts[i]->getFrameOffsetA() * connectedParts[i]->getFrameOffsetB().inverse())
		);
	}

	return 0;
}

int L_Convex::L_SetSize(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetSize() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetSize() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	rigidBody->getCollisionShape()->setLocalScaling(btVector3(x, y, z));

	gameMotionState * motionState = (gameMotionState *)rigidBody->getMotionState();
	for(unsigned int i=0; i<motionState->sceneNodes.size(); i++){
		motionState->sceneNodes[i].first->setScale(core::vector3df(x, y, z));
	}

	return 0;
}

int L_Convex::L_SetMass(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2){
			lua_pushstring(L, "Convex:SetMass() accepts one number.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2)){
			lua_pushstring(L, "Convex:SetMass() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	btScalar mass = lua_tonumber(L, 2);
	bodyMass = lua_tonumber(L, 2);

	rigidBody->setMassProps(mass, rigidBody->getLocalInertia());

	return 0;
}

int L_Convex::L_AddModel(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=2 && lua_gettop(L)!=5 && lua_gettop(L) != 8){
			lua_pushstring(L, "Convex:AddModel() accepts a model and/or 3 coordinates and/or 3 euler angles");
			lua_error(L);

			return 0;
		}
	#endif

	float x=0, y=0, z=0, ex=0, ey=0, ez=0;

	if(lua_gettop(L)>=5){
		x = lua_tonumber(L, 3);
		y = lua_tonumber(L, 4);
		z = lua_tonumber(L, 5);

		if(lua_gettop(L) > 5) {
			ex = lua_tonumber(L, 6);
			ey = lua_tonumber(L, 7);
			ez = lua_tonumber(L, 8);
		}

		lua_pop(L, lua_gettop(L)-2);
	}

	btTransform tf;
	tf.setIdentity();
	tf.setOrigin(btVector3(x, y, z));

	btTransform tf2;
	btQuaternion qt;
	qt.setEuler(ex, ey, ez);
	tf2.setIdentity();
	tf2.setRotation(qt);

	lua_pushinteger(L, 0);
	lua_gettable(L, 2);

	L_Model** obj = static_cast<L_Model**>(luaL_checkudata(L, -1, "Model"));
  lua_remove(L, -1);

	(*obj)->motionState = (gameMotionState *)rigidBody->getMotionState();

  ((gameMotionState *)rigidBody->getMotionState())->addSceneNode((*obj)->sceneNode, tf2*tf);

  lua_getglobal(L, "SetParent");
  lua_pushvalue(L, 2);
  lua_pushvalue(L, 1);
  lua_call(L, 2, LUA_MULTRET);

    return 0;
}

int L_Convex::L_ApplyImpulse(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:ApplyImpulse() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:ApplyImpulse() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);


	btVector3 relativeForce = btVector3(x,y,z);
	rigidBody->applyCentralImpulse(relativeForce);

	return 0;
}

int L_Convex::L_ToWorld(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:ToWorld() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:ToWorld() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	btVector3 localVector = btVector3(x, y, z);

	lua_pop(L, 4);

	btVector3 tf = rigidBody->getCenterOfMassTransform() * localVector;

	lua_pushnumber(L, tf.x());
	lua_pushnumber(L, tf.y());
	lua_pushnumber(L, tf.z());

	return 3;
}

int L_Convex::L_ToObject(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:ToObject() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:ToObject() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	btVector3 localVector = btVector3(x, y, z);

	lua_pop(L, 4);

	btVector3 tf = rigidBody->getCenterOfMassTransform().inverse() * localVector;

	lua_pushnumber(L, tf.x());
	lua_pushnumber(L, tf.y());
	lua_pushnumber(L, tf.z());

	return 3;
}

int L_Convex::L_SetVelocity(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetVelocity() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetVelocity() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	btVector3 localVector = btVector3(x, y, z);

	rigidBody->setLinearVelocity(localVector);

	return 0;
}

int L_Convex::L_SetAngularVelocity(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetAngularVelocity() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetAngularVelocity() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	btVector3 localVector = btVector3(x, y, z);

	rigidBody->setAngularVelocity(localVector);

	return 0;
}

int L_Convex::L_GetPosition(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=1){
			lua_pushstring(L, "Convex:GetPosition() takes no arguments.");
			lua_error(L);

			return 0;
		}
	#endif

	lua_pop(L, 1);

	btVector3 tf = rigidBody->getCenterOfMassTransform().getOrigin();

	lua_pushnumber(L, tf.x());
	lua_pushnumber(L, tf.y());
	lua_pushnumber(L, tf.z());

	return 3;
}

void L_Convex::collided(lua_State *L) {
	if(collisionDetected != NULL) {
		collisionDetected->Fire(L, 0);
	}
}


int L_Convex::L_SetGravity(lua_State *L){
	#if LUA_CHECK_ERRORS
		if(lua_gettop(L)!=4){
			lua_pushstring(L, "Convex:SetGravity() accepts three numbers.");
			lua_error(L);

			return 0;
		}
	#endif

	#if LUA_CHECK_ERRORS
		if(!lua_isnumber(L, 2) || !lua_isnumber(L, 3) || !lua_isnumber(L, 4)){
			lua_pushstring(L, "Convex:SetGravity() accepts numbers only.");
			lua_error(L);

			return 0;
		}
	#endif

	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);

	btVector3 localVector = btVector3(x, y, z);

	rigidBody->setGravity(localVector);

	return 0;
}
