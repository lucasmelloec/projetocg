///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "GameTimer.h"

GameTimer::GameTimer() : Seconds(0), Minutes(0), Delta(0), NewTime(0), OldTime(0), Elapsed(0), Counter(0)
{
}

GameTimer::~GameTimer()
{
}

void GameTimer::init(ITimer* t, f32 timeToWait)
{
   timer = t;
   OldTime = timer->getRealTime();

   this->timeToWait = timeToWait;
}

void GameTimer::update()
{
   NewTime = timer->getRealTime();
   Elapsed = (NewTime - OldTime);
   OldTime = NewTime;

   Counter += Elapsed;
   if(Counter >= 1000)
   {
      Counter -= 1000;
      Seconds+=1;
   }
}

bool GameTimer::timeHasPassed()
{
  return getSecondsElapsed() >= timeToWait;
}

s32 GameTimer::getSecondsElapsed()
{
   return Seconds;
}

f32 GameTimer::getMinutesElapsed()
{
   return Seconds/60.0f;
}

f32 GameTimer::getDeltaTime()
{
   return Elapsed/1000.0f;
}
