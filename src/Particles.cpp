///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "Particles.h"
#include "Globals.h"

std::vector<std::pair<GameTimer*, scene::IParticleSystemSceneNode*>> Particles::timers;
std::map<int, scene::IParticleSystemSceneNode*> Particles::particleNodes;
int Particles::particleNodesCounter = 0;

void Particles::initializeParticles(lua_State *L)
{
    lua_newtable(L);
    lua_pushvalue(L, -1);

    // Namespace
    lua_setglobal(L, "Particles");

    // Exposed Methods
    lua_pushstring(L, "Explode");
    lua_pushcfunction(L, &Particles::explosionParticles);
    lua_settable(L, -3);

    lua_pushstring(L, "Destroy");
    lua_pushcfunction(L, &Particles::destroyParticles);
    lua_settable(L, -3);

    lua_pushstring(L, "ChangeColor");
    lua_pushcfunction(L, &Particles::changeColor);
    lua_settable(L, -3);

    lua_pop(L, 1);
}

int Particles::explosionParticles(lua_State *L)
{
    // Usage: Explode(x, y, z, size, radius, timeToWait, [initialColor, endColor, forever?, angle, smoke?])
    float pos_x = (float)lua_tonumber(L, 1);
    float pos_y = (float)lua_tonumber(L, 2);
    float pos_z = (float)lua_tonumber(L, 3);
    float size = (float)lua_tonumber(L, 4);
    float radius = (float)lua_tonumber(L, 5);
    float timeToWait = (float)lua_tonumber(L, 6);
    int colorR = 255;
    int colorG = 153;
    int colorB = 0;
    int colorR2 = 255;
    int colorG2 = 68;
    int colorB2 = 24;
    bool forever = false;
    float angle = 180;
    bool smoke = true;

    if(lua_gettop(L) >= 12) {
      colorR = (int)lua_tointeger(L, 7);
      colorG = (int)lua_tointeger(L, 8);
      colorB = (int)lua_tointeger(L, 9);
      colorR2 = (int)lua_tointeger(L, 10);
      colorG2 = (int)lua_tointeger(L, 11);
      colorB2 = (int)lua_tointeger(L, 12);
    }

    if(lua_gettop(L) >= 13) {
      forever = (bool)lua_toboolean(L, 13);
    }

    if(lua_gettop(L) >= 14) {
      angle = (float)lua_tonumber(L, 14);
    }

    if(lua_gettop(L) >= 15) {
      smoke = (bool)lua_toboolean(L, 15);
    }

    // create a particle system

    scene::IParticleSystemSceneNode* ps =
            irrmanager->addParticleSystemSceneNode(false);

    scene::IParticleEmitter* em = ps->createSphereEmitter(
            core::vector3df(0.0, 0.0, 0.0),         // Center
            radius,                                 // Radius
            core::vector3df(0.0f,0.004f,0.0f),      // Direction
            60, 80,                                 // Emit Rate
            video::SColor(255, colorR, colorG, colorB),        // Darkest Color
            video::SColor(255, colorR, colorG, colorB),        // Bightest Color
            timeToWait*1000/15, timeToWait*1000, angle,        // min max age, angle
            core::dimension2df(1.0f, 1.0f),         // Min Size
            core::dimension2df(2.0f, 2.0f));        // Max Size

    ps->setEmitter(em); // this grabs the emitter
    em->drop(); // so we can drop it here without deleting it

    scene::IParticleAffector* paf = ps->createFadeOutParticleAffector(video::SColor(127, colorR2, colorG2, colorB2), timeToWait*500);

    ps->addAffector(paf); // same goes for the affector
    paf->drop();

    ps->setPosition(core::vector3df(pos_x,pos_y,pos_z));
    ps->setScale(core::vector3df(size,size,size));
    ps->setMaterialFlag(video::EMF_LIGHTING, false);
    ps->setMaterialTexture( 0, irrdriver->getTexture("assets/textures/fire.png") );
    ps->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);

    if(!forever) {
      GameTimer *t = new GameTimer();
      t->init(device->getTimer(), timeToWait);

      Particles::timers.push_back(std::pair<GameTimer*, scene::IParticleSystemSceneNode*>(t, ps));
    }


    if(smoke)
    {
      // SMOKE

      scene::IParticleSystemSceneNode* ps2 =
              irrmanager->addParticleSystemSceneNode(false);

      scene::IParticleEmitter* em2 = ps2->createSphereEmitter(
              core::vector3df(0.0, 1.4, 0.0),         // Center
              radius,                                 // Radius
              core::vector3df(0.0f,0.002f,0.0f),      // Direction
              50, 70,                                 // Emit Rate
              video::SColor(255, 127, 127, 127),        // Darkest Color
              video::SColor(255, 200, 200, 200),        // Bightest Color
              timeToWait*100, timeToWait*1500, angle,   // min max age, angle
              core::dimension2df(0.7f, 0.7f),         // Min Size
              core::dimension2df(1.3f, 1.3f));        // Max Size

      ps2->setEmitter(em2); // this grabs the emitter
      em2->drop(); // so we can drop it here without deleting it

      scene::IParticleAffector* paf2 = ps2->createFadeOutParticleAffector(video::SColor(127, 50, 50, 50), 600);

      ps2->addAffector(paf2); // same goes for the affector
      paf2->drop();

      ps2->setPosition(core::vector3df(pos_x,pos_y,pos_z));
      ps2->setScale(core::vector3df(size,size,size));
      ps2->setMaterialFlag(video::EMF_LIGHTING, false);
      ps2->setMaterialTexture( 0, irrdriver->getTexture("assets/textures/smoke.png") );
      ps2->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);

      if(!forever) {
        GameTimer *t2 = new GameTimer();
        t2->init(device->getTimer(), timeToWait);

        Particles::timers.push_back(std::pair<GameTimer*, scene::IParticleSystemSceneNode*>(t2, ps2));
      }
    }

    if(forever) {
      particleNodes[particleNodesCounter++] = ps;

      lua_pushinteger(L, particleNodesCounter - 1);
    }
    else {
      lua_pushinteger(L, -1);
    }

    return 1;
}

int Particles::destroyParticles(lua_State *L)
{
    int pos = (int)lua_tointeger(L, 1);

    std::map<int, scene::IParticleSystemSceneNode*>::iterator it;
    it = particleNodes.find(pos);

    if(it != particleNodes.end()) {
      it->second->setEmitter(NULL);

      GameTimer *t = new GameTimer();
      t->init(device->getTimer(), 2000);

      Particles::timers.push_back(std::pair<GameTimer*, scene::IParticleSystemSceneNode*>(t, it->second));

      particleNodes.erase(it);
    }

    return 0;
}

int Particles::changeColor(lua_State *L)
{
    int pos = (int)lua_tointeger(L, 1);
    int r = (int)lua_tointeger(L, 2);
    int g = (int)lua_tointeger(L, 3);
    int b = (int)lua_tointeger(L, 4);

    int r2 = (int)lua_tointeger(L, 5);
    int g2 = (int)lua_tointeger(L, 6);
    int b2 = (int)lua_tointeger(L, 7);

    std::map<int, scene::IParticleSystemSceneNode*>::iterator it;
    it = particleNodes.find(pos);

    if(it != particleNodes.end()) {
      it->second->getEmitter()->setMinStartColor(video::SColor(255, r, g, b));
      it->second->getEmitter()->setMaxStartColor(video::SColor(255, r, g, b));

      scene::IParticleAffector *affector = *(it->second->getAffectors().getLast());
      static_cast<scene::IParticleFadeOutAffector*>(affector)->setTargetColor(video::SColor(127, r2, g2, b2));
    }

    return 0;
}

void Particles::update()
{
  std::vector<std::pair<GameTimer*, scene::IParticleSystemSceneNode*>>::iterator it;
  for(it = Particles::timers.begin(); it != Particles::timers.end();)
  {
    it->first->update();
    if(it->first->timeHasPassed()) {
      if(it->second->getEmitter() != NULL) {
        it->second->setEmitter(NULL);
        it->first->init(device->getTimer(), 2);
      }
      else {
        it->second->remove();
        delete it->first;
        it = Particles::timers.erase(it);
      }
    }
    else {
      it++;
    }
  }
}
