///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#include "InputHandler.h"

InputHandler inputHandler;

bool InputHandler::OnEvent(const SEvent& event)
{
	// Remember whether each key is down or up
	if (event.EventType == irr::EET_KEY_INPUT_EVENT){
		if(KeyIsDown[event.KeyInput.Key] != event.KeyInput.PressedDown){
			EventUnion eventData;
			eventData.Key.key = event.KeyInput.Key;
			eventData.Key.down = event.KeyInput.PressedDown;

			EventQueue.push(make_pair(
				event.KeyInput.PressedDown ? OnKeyDown : OnKeyUp,
				eventData
			));
		}

		KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
	}else if(event.EventType == irr::EET_MOUSE_INPUT_EVENT){
		if(event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN){
			EventUnion eventData;
			eventData.MousePos = {
				event.MouseInput.X,
				event.MouseInput.Y
			};

			EventQueue.push(make_pair(
				MouseButton1Down,
				eventData
			));
		}else if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP){
			EventUnion eventData;
			eventData.MousePos = {
				event.MouseInput.X,
				event.MouseInput.Y
			};

			EventQueue.push(make_pair(
				MouseButton1Up,
				eventData
			));
		}else if(event.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN){
			EventUnion eventData;
			eventData.MousePos = {
				event.MouseInput.X,
				event.MouseInput.Y
			};

			EventQueue.push(make_pair(
				MouseButton2Down,
				eventData
			));
		}else if(event.MouseInput.Event == irr::EMIE_RMOUSE_LEFT_UP){
			EventUnion eventData;
			eventData.MousePos = {
				event.MouseInput.X,
				event.MouseInput.Y
			};

			EventQueue.push(make_pair(
				MouseButton2Up,
				eventData
			));
		}else if(event.MouseInput.Event == irr::EMIE_MOUSE_MOVED){
			EventUnion eventData;
			eventData.MousePos = {
				event.MouseInput.X,
				event.MouseInput.Y
			};

			EventQueue.push(make_pair(
				MouseMoved,
				eventData
			));
		}
	}


		/*	if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
		{
			switch(event.MouseInput.Event)
			{
			case EMIE_LMOUSE_PRESSED_DOWN:
				MouseState.LeftButtonDown = true;
				break;

			case EMIE_LMOUSE_LEFT_UP:
				MouseState.LeftButtonDown = false;
				break;

			case EMIE_MOUSE_MOVED:
				MouseState.Position.X = event.MouseInput.X;
				MouseState.Position.Y = event.MouseInput.Y;
				break;

			default:
				// We won't use the wheel
				break;
			}
		}*/

	return false;
}

// This is used to check whether a key is being held down
bool InputHandler::IsKeyDown(EKEY_CODE keyCode) const
{
	return KeyIsDown[keyCode];
}

InputHandler::InputHandler()
{
	for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i)
		KeyIsDown[i] = false;

	OnKeyUp = NULL;
	OnKeyDown = NULL;
}

void InputHandler::Destroy(lua_State * L){
	if(OnKeyUp!=NULL){
		OnKeyUp->Destroy(L);
		delete OnKeyUp;
	}
	if(OnKeyDown!=NULL){
		OnKeyDown->Destroy(L);
		delete OnKeyDown;
	}
	if(MouseButton1Down!=NULL){
		MouseButton1Down->Destroy(L);
		delete MouseButton1Down;
	}
	if(MouseButton1Up!=NULL){
		MouseButton1Up->Destroy(L);
		delete MouseButton1Up;
	}
	if(MouseButton2Down!=NULL){
		MouseButton2Down->Destroy(L);
		delete MouseButton2Down;
	}
	if(MouseButton2Up!=NULL){
		MouseButton2Up->Destroy(L);
		delete MouseButton2Up;
	}
	if(MouseMoved!=NULL){
		MouseMoved->Destroy(L);
		delete MouseMoved;
	}
}

void InputHandler::initializeLua(lua_State * L){
	mainState = L;

	lua_newtable(L);
	lua_pushvalue(L, -1);
	lua_setglobal(L, "Input");

	OnKeyUp = new LuaEvent(L, "OnKeyUp");
	OnKeyDown = new LuaEvent(L, "OnKeyDown");

	MouseButton1Down = new LuaEvent(L, "MouseButton1Down");
	MouseButton1Up = new LuaEvent(L, "MouseButton1Up");

	MouseButton2Down = new LuaEvent(L, "MouseButton2Down");
	MouseButton2Up = new LuaEvent(L, "MouseButton2Up");

	MouseMoved = new LuaEvent(L, "MouseMoved");

	lua_pop(L, 1);
}

void InputHandler::handleEvents(lua_State * L){
	while(!EventQueue.empty()){
		EventUnion eventData = EventQueue.front().second;
		LuaEvent * eventPointer = EventQueue.front().first;

		if(eventPointer == OnKeyDown){
			lua_pushinteger(L, eventData.Key.key);
			OnKeyDown->Fire(L, 1);
		}else if(eventPointer == OnKeyUp){
			lua_pushinteger(L, eventData.Key.key);
			OnKeyUp->Fire(L, 1);
		}else if(eventPointer == MouseButton1Down){
			lua_pushinteger(L, eventData.MousePos.x);
			lua_pushinteger(L, eventData.MousePos.y);
			MouseButton1Down->Fire(L, 2);
		}else if(eventPointer == MouseButton1Up){
			lua_pushinteger(L, eventData.MousePos.x);
			lua_pushinteger(L, eventData.MousePos.y);
			MouseButton1Up->Fire(L, 2);
		}else if(eventPointer == MouseButton2Down){
			lua_pushinteger(L, eventData.MousePos.x);
			lua_pushinteger(L, eventData.MousePos.y);
			MouseButton2Down->Fire(L, 2);
		}else if(eventPointer == MouseButton2Up){
			lua_pushinteger(L, eventData.MousePos.x);
			lua_pushinteger(L, eventData.MousePos.y);
			MouseButton2Up->Fire(L, 2);
		}else if(eventPointer == MouseMoved){
			lua_pushinteger(L, eventData.MousePos.x);
			lua_pushinteger(L, eventData.MousePos.y);
			MouseMoved->Fire(L, 2);
		}else{
			// Do nothing
		}

		EventQueue.pop();
	}
}
