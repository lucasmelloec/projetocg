-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

print("Starting ScriptManager.lua...")

glib.Run(function()

	wait()

	shared.game = Object()

    glib.Run(dofile,"scripts/InitialSetup.lua")

    dofile("scripts/Map.lua")

    shared.workspace = Object()
    shared.workspace:SetParent(game)

    Input.OnKeyDown:connect(print)

    -- General Scripts
    glib.Run(dofile, "scripts/Player.lua")

    repeat
		local t = tick()/3

		wait()
	until false

	--l.event:fire("Event Hello!")
end)
