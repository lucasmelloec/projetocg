-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Tank = require("./scripts/Tank")
local Agent = require("./scripts/Agent")
local GameLogic = require("./scripts/GameLogic")
local Pathfinding = require("./scripts/Pathfinding")

local playerTank
local SpawnPlayerTank, DelaySpawn

function DelaySpawn()
    playerTank = nil
    shared.player = nil

    wait(5)

    SpawnPlayerTank()
end

function SpawnPlayerTank()
    playerTank = Tank:new("r")
    playerTank:SetPosition(0, 3, 0)
    playerTank:SetRotation(0, 0, 0)

    playerTank.Team = shared.TeamRed
    shared.player = playerTank

    playerTank.Died:connect(DelaySpawn)
end

SpawnPlayerTank()

-- local agent = Agent:new()
-- agent:SetPosition(20, 3, 0)
-- agent:SetRotation(0, 0, 0)
-- agent.Team =

local Angle = 0
local Direction = 0

local KeysPressed = {}

Input.OnKeyDown:connect(
    function(key)
        KeysPressed[key] = true
    end
)

Input.OnKeyUp:connect(
    function(key)
        KeysPressed[key] = false
    end
)

Input.MouseButton1Down:connect(
    function(x, y)
        if not playerTank then return end
        playerTank:Shoot();
    end
)

local function UpdateMovement()
    if not playerTank then return end
    if Direction ~= 0 then
        if Direction > 0 then
            playerTank:MoveForward()

            if Angle ~= 0 then
                if Angle > 0 then
                    playerTank:TurnRight()
                else
                    playerTank:TurnLeft()
                end
            end
        else
            playerTank:MoveBackwards()

            if Angle ~= 0 then
                if Angle > 0 then
                    playerTank:TurnRight()
                else
                    playerTank:TurnLeft()
                end
            end
        end
    else
        playerTank:Stop()
    end
end

UpdateMovement()

Input.OnKeyDown:connect(function(key, ...)
    if not playerTank then return end
    if key == 50 then
        print(playerTank.LocalTank.body:GetPosition())
    end
    if key == 51 then
        local x, y, z = playerTank.LocalTank.body:GetPosition()

        -- print(Pathfinding:GetWaypoints(100, 2031, x, z))
        agent:GuidedMove(x, z)
    end

    if key == Enum.Keys.w then
        Direction = 1
    elseif key == Enum.Keys.s then
        Direction = -1
    elseif(key == Enum.Keys.a) then
        Angle = -1
    elseif(key == Enum.Keys.d) then
        Angle = 1
    end

    UpdateMovement()
end)

Input.OnKeyUp:connect(function(key, ...)
    if not playerTank then return end
    if key == Enum.Keys.w then
        if KeysPressed[Enum.Keys.s] then
            Direction = -1
        else
            Direction = 0
        end
    elseif key == Enum.Keys.s then
        if KeysPressed[Enum.Keys.w] then
            Direction = 1
        else
            Direction = 0
        end
    elseif key == Enum.Keys.a then
        if KeysPressed[Enum.Keys.d] then
            Angle = 1
        else
            Angle = 0
        end
    elseif key == Enum.Keys.d then
        if KeysPressed[Enum.Keys.a] then
            Angle = -1
        else
            Angle = 0
        end
    end

    UpdateMovement()
end)

Input.MouseMoved:connect(function(x, y)
    if not playerTank then return end
    local mx, my = GetWindowSize()
    playerTank:SetTarget((x-mx/2)/(mx/2), (my-y)/my)
end)

--repeat
--    wait()
--until false
