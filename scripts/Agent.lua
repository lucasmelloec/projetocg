-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Tank = require("./scripts/Tank")
local Pathfinding = require("./scripts/Pathfinding")
local Vector = require("./scripts/Vector")

local Agent = Tank:inherit()

local minimumDistance = 12

local EngageDistance = 30

local minDistance = 10
local maxDistance = 20

function countk(t)
	local count = 0

	for _, _ in pairs(t) do
		count = count + 1
	end

	return count
end

function Agent:constructor(team, prefix)
	self:super(prefix)

	self.Behavior = "Stopped"
	self.Team = team

	glib.Run(
		function()
			wait(0.5)

			repeat
				if self.Health == 0 then
					return
				end

				if self.Behavior == "Follow" then
					local tx, ty, tz = self.Who.LocalTank.body:GetPosition()
					self:SimpleMove(tx, tz)

					wait(0.25)
				elseif self.Behavior == "Stopped" then
					self:Stop()

					wait(0.2)
				elseif self.Behavior == "Reachpoint" then
					while #self.Waypoints > 0 and self:SimpleMove(self.Waypoints[1].x, self.Waypoints[1].y) do
						table.remove(self.Waypoints, 1)
					end

					if #self.Waypoints == 0 then
						self.Behavior = "Stopped"
					end

					wait(0.2)
				else
					wait(0.2)
				end
			until false
		end
	)

	glib.Run(
		function()
			wait(0.5)

			repeat
				if self.Dead then
					return
				end

				local Target, mindist

				for k, v in pairs(shared.Tanks) do
					if v.Team ~= self.Team and not v.Dead then
						local distance = (Vector:new(self.LocalTank.body:GetPosition()) - Vector:new(v.LocalTank.body:GetPosition())):mag2()
						if distance < EngageDistance*EngageDistance then
							if Target then
								if distance < mindist then
									Target = v
									mindist = distance
								end
							else
								Target = v
								mindist = distance
							end
						end
					end
				end

				self.Target = Target

				wait(2.5)
			until false
		end
	)

	glib.Run(
		function()
			wait(0.5)

			repeat
				if self.Dead then
					return
				end

				if not self.Target then
					if not self.ToCapture or self.ToCapture.Owner == self.Team then
						if self.ToCapture and self.ToCapture.Owner == self.Team then
							self.Team.Control[self.ToCapture][self] = nil
						end

						local points
						local capture

						for k, v in pairs(shared.ControlPoints) do
							if v.Owner ~= self.Team then
								local cpoints = countk(self.Team.Control[v]) + (v.Position - Vector:new(self.LocalTank.body:GetPosition())):mag()/40
								if capture then
									if cpoints < points then
										capture = v
										points = cpoints
									end
								else
									capture = v
									points = cpoints
								end
							end
						end

						if capture then
							self.ToCapture = capture
							self.Team.Control[capture][self] = true
						end
					end

					if self.ToCapture then
						self:GuidedMove(self.ToCapture.Position.x, self.ToCapture.Position.z)
					end
				else
					repeat
						if self.Target.Dead or self.Dead then
							self.Target = nil
							break
						end

						self.Behavior = "Persuit"

						local targetDistance = (Vector:new(self.LocalTank.body:GetPosition()) - Vector:new(self.Target.LocalTank.body:GetPosition())):mag()

						local angle
						local isBehind

						do
							local nx, nz

							local ux, uy, uz = self.LocalTank.body:ToObject(self.Target.LocalTank.body:GetPosition())
							local mag = math.sqrt(ux*ux+uz*uz)

							nx = ux/mag
							nz = uz/mag

							angle = -math.asin(nx)
							isBehind = nz < 0
						end

						local tx, ty, tz = self.Target.LocalTank.body:GetPosition()

						if not isBehind then
							self:SetTarget(-angle, math.pi/16)
						end

						if targetDistance > maxDistance then
							self:SimpleMove(tx, tz)
						else
							if targetDistance < minDistance then
								if isBehind then
									self:MoveForward()

									if angle > 0 then
										self:TurnRight()
									else
										self:TurnLeft()
									end
								else
									self:MoveBackwards()

									if angle > 0 then
										self:TurnRight()
									else
										self:TurnLeft()
									end
								end
							else
								if isBehind then
									self:MoveForward()

									if angle > 0 then
										self:TurnRight()
									else
										self:TurnLeft()
									end
								else
									if math.abs(angle) > math.pi/4 then
										self:MoveBackwards()

										if angle > 0 then
											self:TurnRight()
										else
											self:TurnLeft()
										end
									else
										self:Shoot()
										--print("Shoot")
										self:Stop()
									end

								end
							end
						end

						wait(0.25)
					until not self.Target

					self.Behavior = "Stopped"
				end

				wait(2.5)
			until false
		end
	)
end

function Agent:GuidedMove(x, y)
	local cx, cy, cz = self.LocalTank.body:GetPosition()
	local waypoints = Pathfinding:GetWaypoints(cx, cz, x, y)

	if waypoints then
		self.Waypoints = waypoints
		self.Behavior = "Reachpoint"
	end
end

function Agent:SimpleMove(x, z)
	-- Posição do tanque (current)
	local cx, cy, cz = self.LocalTank.body:GetPosition()

	local dx = cx-x
	local dz = cz-z

	if dx*dx + dz*dz > minimumDistance*minimumDistance then
		local nx, nz

		do
			local ix, iy, iz = self.LocalTank.body:ToObject(x, 0, z)

			local mag = math.sqrt(ix*ix + iz*iz)
			nx = ix/mag
			nz = iz/mag
		end

		local angle = -math.asin(nx)
		local side = nz

		self:MoveForward()
		if math.abs(angle) > math.pi/12 or side < 0 then
			if angle > 0 then
				self:TurnLeft()
			else
				self:TurnRight()
			end
		end
		return false
	else
		self:Stop()
		return true
	end
end

function Agent:Follow(who)
	self.Who = who
	self.Behavior = "Follow"
end

return Agent
