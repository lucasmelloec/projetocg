-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

print("Starting wait.lua")

local heap = require("./scripts/heap")

local waitHeap = heap:new()

function wait(waitTime)
	if type(waitTime) ~= "number" or waitTime < 0 then
		waitTime = 0.0001
	end

	waitHeap:insert(tick()+waitTime, coroutine.running())
	coroutine.yield()
end

function resumeWait()
	local currentTime = tick()

	while not waitHeap:empty() and waitHeap:next_key() < currentTime do
		coroutine.resume(waitHeap:next_value())
		waitHeap:pop()
	end
end
