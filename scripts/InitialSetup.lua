-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local camera = Camera()
camera:SetParent(shared.game)
local godMode = false

Input.OnKeyDown:connect(function(key,...)
    if(key == Enum.Keys.F1 or key==49) then
        if shared.player then
          shared.game.ChildrenByName.Camera:DebugCamera()

          if godMode then
            camera:Follow(shared.player.LocalTank.body.modelbody)
            camera:SetPosition(0, 5.4, -9)
            local tx, ty, tz = shared.player.LocalTank.body.modelbody:GetPosition()
            camera:SetTarget(tx, ty+5, tz)
          end

          godMode = not godMode
        end
    end
end)

shared.Tanks = {}
shared.ControlPoints = {}
shared.MapScale = 25

setmetatable(shared.Tanks, {__mode = "v"})

-- local dl = Light()
-- dl:SetLightType(Enum.LightType.Directional)
-- dl:SetPosition(-1, 1, -1)
-- dl:SetDiffuseColor(1, 1, 1)
-- dl:SetParent(shared.game)

local dr = 0.5

local dl = Light()
dl:SetLightType(Enum.LightType.Directional)
dl:SetPosition(1, 1, 1)
dl:SetDiffuseColor(1, 1, 1)
dl:SetParent(shared.game)

local dl2 = Light()
dl2:SetLightType(Enum.LightType.Directional)
dl2:SetPosition(-1, 1, -1)
dl2:SetDiffuseColor(dr, dr,dr)
dl2:SetParent(shared.game)

local dl1 = Light()
dl1:SetLightType(Enum.LightType.Directional)
dl1:SetPosition(-1, 1, 1)
dl1:SetDiffuseColor(dr, dr, dr)
dl1:SetParent(shared.game)

local dl3 = Light()
dl3:SetLightType(Enum.LightType.Directional)
dl3:SetPosition(1, 1, -1)
dl3:SetDiffuseColor(dr, dr, dr)
dl3:SetParent(shared.game)

local lastPlayer = shared.Player

local inOrigin = false

repeat
    if shared.player then
        if shared.player ~= lastPlayer then
            if inOrigin and not godMode then
                inOrigin = false
                camera:DebugCamera()

                camera:Follow(shared.player.LocalTank.body.modelbody)
                camera:SetPosition(0, 5.4, -9)
            end

            lastPlayer = shared.Player
        end

        local tx, ty, tz = shared.player.LocalTank.body.modelbody:GetPosition()
        camera:SetTarget(tx, ty+5, tz)
    else
        if not inOrigin and not godMode then
            inOrigin = true

            camera:DebugCamera()
            camera:SetPosition(0, 10, 0)
        end
    end

    wait()

until false
