-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Agent = require("./scripts/Agent")
local Vector = require("./scripts/Vector")
local ControlPoint = require("./scripts/ControlPoint")

local MaxAgents = 5

local SpawnPoints = {
	Vector:new(-6, 8, -10),
	Vector:new(-6, 8, -10)
}

local a = Vector:new(-22, 0,  36)*3

table.insert(shared.ControlPoints, ControlPoint:new(Vector:new(-22, 0,  36)*(shared.MapScale/15)))
table.insert(shared.ControlPoints, ControlPoint:new(Vector:new( 90, -3,  62)*(shared.MapScale/15)))
table.insert(shared.ControlPoints, ControlPoint:new(Vector:new( 44, 0, -39)*(shared.MapScale/15)))
table.insert(shared.ControlPoints, ControlPoint:new(Vector:new(-72, 10.8, -72)*(shared.MapScale/15)))

ControlPoints = shared.ControlPoints

local Team = glib.OO:inherit()

local AllTanks = shared.Tanks

function Team:constructor(prefix, initialColor, endColor)
	self.Agents = {}
	self.SpawnPoints = {}
	self.Control = {}

	self.initialColor = initialColor
	self.endColor = endColor

	for _, controlPoint in pairs(shared.ControlPoints) do
		self.Control[controlPoint] = {}
		setmetatable(self.Control[controlPoint], {__mode = "k"})
	end

	local AgentCount = 0

	local function DecrementAgentCount()
		AgentCount = AgentCount - 1
	end

	glib.Run(
		function()
			wait(1)

			repeat
				if AgentCount < MaxAgents then
					AgentCount = AgentCount + 1

					local newAgent = Agent:new(self, prefix)

					newAgent.LocalTank.body:SetPosition(self.SpawnPoints[math.random(#self.SpawnPoints)]:unpack())
					newAgent.LocalTank.body:SetRotation(0, 0, 0)

					newAgent.Died:connect(DecrementAgentCount)
				end

				wait(5)
			until false
		end
	)
end

function Team:AddSpawnPoint(spawn)
	table.insert(self.SpawnPoints, spawn)
end

local TeamRed = Team:new("r", Vector:new(200, 0, 0), Vector:new(160, 0, 0))

TeamRed:AddSpawnPoint((Vector:new(-73, 1, 78)*(shared.MapScale/15)) + Vector:new(0, 1, 0))

local TeamBlue = Team:new("", Vector:new(0, 0, 200), Vector:new(0, 0, 160))

TeamBlue:AddSpawnPoint((Vector:new(85, 1, -84)*(shared.MapScale/15)) + Vector:new(0, 1, 0))

shared.TeamRed = TeamRed
shared.TeamBlue = TeamBlue

print("Team red:", TeamRed)
print("Team blue:", TeamBlue)
