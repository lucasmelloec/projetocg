-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Vector = require("./scripts/Vector")

local Tank = glib.OO:inherit()

local xlimit = math.pi/4
local ylimit = math.pi/16

local offset = math.pi/2

local function limitx(x)
    return math.max(-xlimit, math.min(x, xlimit)) + offset
end

local function limity(x)
    return math.max(-ylimit, math.min(x, ylimit))
end

local maxMotorImpulse = 0.15

local CannonCooldown = 0.45

function Tank:constructor(prefix)
    self.LocalTank = Object()
    local tank = self.LocalTank

    tank:SetName("Tank")
    tank:SetParent(shared.workspace)

    local wheelSettings = {
        friction = 1.5
    }

    local bodyMass = 5
    local wheelMass = 5

    self.TankParts = {}

    tank.body = Convex("assets/models/cube.obj")
    -- tank.body.model = Model("assets/models/cube.obj")
    tank.body.modelbody = Model("assets/models/tank/" .. (prefix or "") .. "tank_body.obj")
    -- 3.32967
    tank.body.modelwheelsl = Model("assets/models/tank/tank_wheels_l.obj")
    tank.body.modelwheelsr = Model("assets/models/tank/tank_wheels_r.obj")
    tank.body:AddModel(tank.body.modelbody, 0, -1.4, 1)
    tank.body:AddModel(tank.body.modelwheelsl, -2.4, -1.3, 1.3)
    tank.body:AddModel(tank.body.modelwheelsr, 2.4, -1.3, 1.3)
    tank.body:SetMass(bodyMass)
    tank.body:SetPosition(0, 20, 0)
    tank.body:SetSize(2, 1.5, 4)
    tank.body.modelbody:SetSize(1, 1, 1);
    tank.body.modelwheelsl:SetSize(1, 1, 1);
    tank.body.modelwheelsr:SetSize(1, 1, 1);
    tank.body:SetParent(tank)

    tank.wheelFL = Convex("assets/models/cylinder.obj", wheelSettings)
    -- tank.wheelFL:AddModel(Model("assets/models/cylinder.obj"))
    tank.wheelFL:SetMass(wheelMass)
    tank.wheelFL:SetSize(1,0.41, 1)
    tank.wheelFL:SetPosition(0, 3, 0)
    tank.wheelFL.hinge = Hinge(tank.body,tank.wheelFL,
        -1.5,-0.75,1.5,0,-0.2,0,
        1,0,0,0,1,0)
    tank.wheelFL.hinge:SetParent(tank.wheelFL)
    tank.wheelFL:SetParent(tank)

    tank.wheelBL = Convex("assets/models/cylinder.obj", wheelSettings)
    -- tank.wheelBL:AddModel(Model("assets/models/cylinder.obj"))
    tank.wheelBL:SetMass(wheelMass)
    tank.wheelBL:SetSize(1,0.41, 1)
    tank.wheelBL:SetPosition(0, 3, 0)
    tank.wheelBL.hinge = Hinge(tank.body,tank.wheelBL,
        -1.5,-0.75,-1.5,0,-0.2,0,
        1,0,0,0,1,0)
    tank.wheelBL.hinge:SetParent(tank.wheelBL)
    tank.wheelBL:SetParent(tank)

    tank.wheelFR = Convex("assets/models/cylinder.obj", wheelSettings)
    -- tank.wheelFR:AddModel(Model("assets/models/cylinder.obj"))
    tank.wheelFR:SetMass(wheelMass)
    tank.wheelFR:SetSize(1,0.41, 1)
    tank.wheelFR:SetPosition(0, 3, 0)
    tank.wheelFR.hinge = Hinge(tank.body,tank.wheelFR,
        1.5,-0.75,1.5,0,0.2,0,
        1,0,0,0,1,0)
    tank.wheelFR.hinge:SetParent(tank.wheelFR)
    tank.wheelFR:SetParent(tank)

    tank.wheelBR = Convex("assets/models/cylinder.obj", wheelSettings)
    -- tank.wheelBR:AddModel(Model("assets/models/cylinder.obj"))
    tank.wheelBR:SetMass(wheelMass)
    tank.wheelBR:SetSize(1,0.41, 1)
    tank.wheelBR:SetPosition(0, 3, 0)
    tank.wheelBR.hinge = Hinge(tank.body,tank.wheelBR,
        1.5,-0.75,-1.5,0,0.2,0,
        1,0,0,0,1,0)
    tank.wheelBR.hinge:SetParent(tank.wheelBR)
    tank.wheelBR:SetParent(tank)

    tank.hardpointMount = Convex("assets/models/cylinder.obj", Enum.Collision.NoCollide, Enum.Collision.Nothing)
    tank.hardpointMount.modelupperbody = Model("assets/models/tank/tank_upperBody.obj")
    -- tank.hardpointMount:AddModel(Model("assets/models/cylinder.obj"))
    tank.hardpointMount:AddModel(tank.hardpointMount.modelupperbody, 0, -2.5, 2, math.pi/2, math.pi, 0)
    tank.hardpointMount:SetMass(0.2)
    tank.hardpointMount:SetSize(2, 0.8, 2)
    tank.hardpointMount.modelupperbody:SetSize(1, 1, 1)
    tank.hardpointMount:SetPosition(0, 3, 0)
    tank.hardpointMount.hinge = Hinge(tank.body, tank.hardpointMount,
        0, 0.75, 0,
        0, 0.3, 0,
        0, 1, 0,
        0, -1, 0
        )
    tank.hardpointMount.hinge:SetLimits(-math.pi/4.0 + offset, math.pi/4.0 + offset)
    tank.hardpointMount.hinge:EnableAngleMotor(maxMotorImpulse)
    tank.hardpointMount.hinge:SetParent(tank.hardpointMount)
    tank.hardpointMount:SetParent(tank)

    tank.cannon = Convex("assets/models/cube.obj", Enum.Collision.NoCollide, Enum.Collision.Nothing)
    tank.cannon.modelshootingpipe = Model("assets/models/tank/tank_shootingPipe.obj")
    -- tank.cannon:AddModel(Model("assets/models/cube.obj"))
    tank.cannon:AddModel(tank.cannon.modelshootingpipe, 0, 0, 0.6, math.pi/2, 0, 0)
    tank.cannon:SetMass(0.2)
    tank.cannon:SetSize(1, 0.4, 0.4)
    tank.cannon.modelshootingpipe:SetSize(1, 1, 1)
    tank.cannon:SetPosition(0, 3, 0)
    tank.cannon.hinge = Hinge(tank.hardpointMount, tank.cannon,
        -1.0, -0.1, 0,
        -0.6, 0, 0,
        0, 0, 1,
        0, 0, -1
        )
    tank.cannon.hinge:SetLimits(-math.pi/16.0, math.pi/16.0)
    tank.cannon.hinge:EnableAngleMotor(maxMotorImpulse)
    tank.cannon.hinge:SetParent(tank.cannon)
    tank.cannon:SetParent(tank)

    self.LinearFactor = 0
    self.AngularVelocity = 0

    self.TargetVelocity = 12
    self.MaximumImpulse = 20

    self.HardpointOffset = offset

    self.Health = 100
    self.Dead = false
    self.Died = glib.Event:new()

    table.insert(shared.Tanks, self)

    self.LastShot = tick()

    self.XTarget = 0
    self.YTarget = 0

    table.insert(self.TankParts, tank.body)
    table.insert(self.TankParts, tank.wheelFR)
    table.insert(self.TankParts, tank.wheelBR)
    table.insert(self.TankParts, tank.wheelBL)
    table.insert(self.TankParts, tank.wheelFL)
    table.insert(self.TankParts, tank.cannon)
    table.insert(self.TankParts, tank.hardpointMount)

    glib.Run(
        function()
            -- for i=1, 5 do
            --     part.body:SetRotation(0, 0, 0)
            --     for _, part in pairs(self.TankParts) do
            --         part:SetAngularVelocity(0, 0, 0)
            --         part:SetVelocity(0, 0, 0)
            --     end

            --     wait(0.1)
            -- end
            -- repeat
            --     tank.body:SetRotation(0, 0, 0)
            --     wait()
            -- until false

            repeat
                self.LocalTank.hardpointMount.hinge:SetAngleMotorTarget(self.XTarget)
                self.LocalTank.cannon.hinge:SetAngleMotorTarget(self.YTarget)

                wait(0.1)

                if not self:IsUpwards() then
                    local start = tick()

                    while not self:IsUpwards() do
                        if tick() > start + 5 then
                            self:TakeDamage(200)
                            break
                        end

                        wait(0.1)
                    end
                end
            until self.Dead
        end
    )

    self:Stop()
end

function Tank:SetTarget(x, y)
    self.XTarget = limitx(math.pi/4.0*x)
    self.YTarget = limity(y*math.pi/4.0)
    -- self.LocalTank.hardpointMount.hinge:SetAngleMotorTarget(self.HardpointOffset + math.pi/4.0*x)
    -- self.LocalTank.cannon.hinge:SetAngleMotorTarget(y*math.pi/4.0)
end

function Tank:SetCannonAngles(x, y)
    self.XTarget = limitx(x)
    self.YTarget = limity(y)
    -- self.LocalTank.hardpointMount.hinge:SetAngleMotorTarget(limitx(x))
    -- self.LocalTank.cannon.hinge:SetAngleMotorTarget(y)
end

function Tank:SetPosition(x, y, z)
    self.LocalTank.body:SetPosition(x, y, z)

    for _, part in pairs(self.TankParts) do
        part:SetAngularVelocity(0, 0, 0)
        part:SetVelocity(0, 0, 0)
    end
end

function Tank:SetRotation(x, y, z)
    self.LocalTank.body:SetRotation(x, y, z)

    for _, part in pairs(self.TankParts) do
        part:SetAngularVelocity(0, 0, 0)
        part:SetVelocity(0, 0, 0)
    end
end

function Tank:MoveForward()
    self.LinearFactor = 1

    self.LocalTank.wheelFR.hinge:SetMotor(self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBR.hinge:SetMotor(self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelFL.hinge:SetMotor(self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBL.hinge:SetMotor(self.TargetVelocity, self.MaximumImpulse)
end

function Tank:MoveBackwards()
    self.LinearFactor = -1

    self.LocalTank.wheelFR.hinge:SetMotor(-self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBR.hinge:SetMotor(-self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelFL.hinge:SetMotor(-self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBL.hinge:SetMotor(-self.TargetVelocity, self.MaximumImpulse)
end

function Tank:TurnLeft()
    if self.LinearFactor == 0 then return end

    self.LocalTank.wheelFR.hinge:SetMotor(self.LinearFactor*self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBR.hinge:SetMotor(self.LinearFactor*self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelFL.hinge:DisableMotor()
    self.LocalTank.wheelBL.hinge:DisableMotor()
end

function Tank:TurnRight()
    if self.LinearFactor == 0 then return end

    self.LocalTank.wheelFR.hinge:DisableMotor()
    self.LocalTank.wheelBR.hinge:DisableMotor()
    self.LocalTank.wheelFL.hinge:SetMotor(self.LinearFactor*self.TargetVelocity, self.MaximumImpulse)
    self.LocalTank.wheelBL.hinge:SetMotor(self.LinearFactor*self.TargetVelocity, self.MaximumImpulse)
end

function Tank:Stop()
    self.LinearFactor = 0

    self.LocalTank.wheelFR.hinge:DisableMotor()
    self.LocalTank.wheelBR.hinge:DisableMotor()
    self.LocalTank.wheelFL.hinge:DisableMotor()
    self.LocalTank.wheelBL.hinge:DisableMotor()
end

function Tank:Shoot()
    if tick() < self.LastShot + CannonCooldown then return end
    self.LastShot = tick()

    local projectile = Convex("assets/models/sphere.obj")
    projectile:AddModel(Model("assets/models/sphere.obj"))
    projectile:SetMass(5.0)
    projectile:SetSize(0.2, 0.2, 0.2)

    projectile:SetPosition(self.LocalTank.cannon:ToWorld(3, 0, 0))
    projectile:SetGravity(0, -30, 0)

    projectile:SetParent(self.LocalTank.body)

    local cx, cy, cz = self.LocalTank.cannon:ToWorld(1.0, 0, 0)
    local tx, ty, tz = self.LocalTank.cannon:GetPosition()

    local dx = cx-tx
    local dy = cy-ty
    local dz = cz-tz

    local mag = 120/math.sqrt(dx*dx+dy*dy+dz*dz)
    projectile:ApplyImpulse(dx*mag, dy*mag, dz*mag)

    local collisionEvent
    collisionEvent = projectile.CollisionDetected:connect(
        function()
            local posx, posy, posz = projectile:GetPosition()
            local radius = 10

            -- ExplodeVolume(posx, posy, posz, radius, 0.1)
            Particles.Explode(posx, posy, posz, 1, radius/20, 1)

            collisionEvent:disconnect()
            projectile:SetPosition(0, -40, 0)
            projectile:SetParent(nil)
            projectile = nil

            local damageRadius = 5

            for k, v in pairs(shared.Tanks) do
                if v.Team ~= self.Team then
                    local distance = (Vector:new(posx, posy, posz) - Vector:new(v.LocalTank.body:GetPosition())):mag()

                    if distance < damageRadius then
                        v:TakeDamage(15 - damageRadius)
                    end
                end
            end
        end
    )
end

function Tank:TakeDamage(value)
    if self.Dead then return end
    self.Health = math.max(self.Health-value, 0)

    if self.Health == 0 then
        self.Dead = true

        for i=1, 6 do
            local px, py, pz = self.LocalTank.body:ToWorld(
                (math.random()-0.5)*4,
                (math.random()-0.5)*4,
                (math.random()-0.5)*4
                )
            Particles.Explode(px, py, pz, 1, 0.5, 1)
        end
        self:SetPosition(0, -40, 0)

        self.Died:fire()

        for k, v in pairs(self.TankParts) do
            if v.hinge then
                v.hinge:Destroy()
            end
            v:SetParent(nil)
        end
        self.LocalTank:SetParent(nil)
    end
end

function Tank:IsUpwards()
    local rx, ry, rz = self.LocalTank.body:ToWorld(0, 1, 0)
    local cx, cy, cz = self.LocalTank.body:GetPosition()

    return ry > cy
end

return Tank
