-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Vector = glib.OO:inherit()

function Vector:constructor(x, y, z)
	self.x = x
	self.y = y
	self.z = z
end

function Vector:mag()
	return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)
end

function Vector:mag2()
	return self.x*self.x + self.y*self.y + self.z*self.z
end

function Vector:unpack()
	return self.x, self.y, self.z
end

getmetatable(Vector).__add = function(a1, a2)
	if type(a1) == "number" then
		local aux = a1
		a1 = a2
		a2 = temp
	end

	if type(a2) == "number" then
		return Vector:new(a1.x + a2, a1.y + a2, a1.z + a2)
	elseif type(a2) == "table" then
		return Vector:new(
			a1.x + a2.x,
			a1.y + a2.y,
			a1.z + a2.z
			)
	end
end

getmetatable(Vector).__sub = function(a1, a2)
	if type(a1) == "number" then
		local aux = a1
		a1 = a2
		a2 = temp
	end

	if type(a2) == "number" then
		return Vector:new(a1.x - a2, a1.y - a2, a1.z - a2)
	elseif type(a2) == "table" then
		return Vector:new(
			a1.x - a2.x,
			a1.y - a2.y,
			a1.z - a2.z
			)
	end
end

getmetatable(Vector).__mul = function(a1, a2)
	if type(a1) == "number" then
		local aux = a1
		a1 = a2
		a2 = temp
	end

	if type(a2) == "number" then
		return Vector:new(a1.x * a2, a1.y * a2, a1.z * a2)
	end
end

getmetatable(Vector).__div = function(a1, a2)
	if type(a1) == "number" then
		local aux = a1
		a1 = a2
		a2 = temp
	end

	if type(a2) == "number" then
		return Vector:new(a1.x / a2, a1.y / a2, a1.z / a2)
	end
end

return Vector
