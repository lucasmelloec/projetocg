---------------------------------------------------------------------------------------------------------------
-- glib - General Game Library --------------------------------------------------------------------------------

--[[
	-- Denilson A. Marques Junior (SirGelatina)

	Current members:

	glib.Run(f, ...)
		Has the same behavior as coroutine.create(coroutine.run(f)), but
		appropriately treats errors. DO NOT USE THE DEFAULT COROUTINE
		LIBRARY ANYWHERE ELSE! It also passes extra arguments to the
		function.

	OO:inherit()
		OO acts as the superclass of all classes. Inherits lets you create
		another class, you just need to set its members and a constructor.
		Example:

		Bank = glib.OO:inherit()

		function Bank:constructor(...)
			self.Balance = 0
		end

		function Bank:printBalance()
			print(self.Balance)
		end

		function Bank:setBalance(x)
			self.Balance = x
		end

		To spawn a new instance:

		newBank = Bank:new()

		Any arguments passed to new will be passed to the constructor method.
		Child classes inherit all methods of its parent. There's no way (and
		possibly no reason) to set variables during class definition, only at
		the constructor.

	Event:new()
		(Event derives from OO)
		Returns an Event object, which has the following members:

		Event:fire(...)
			Executes all bound functions with arguments (...), sequentially

		Event:fireParallel(...)
			Executes all bound functions with arguments (...), concurrently
			(recommended)

		Event:connect(f)
			Binds function f to the event. Returns a table that, in turn,
			contains:

			Event:connect(f).disconnect()
				Disconnects the function

			Event:connect(f).fire(...)
				Calling this is the same as calling f. Arguments passed to this
				method will also be passed to f.

		Event.connections
			DO NOT TOUCH

		Event:purgeConnections()
			Elegantly (more like carelessly) discards all current connections

]]

print("Starting glib.lua")

local auxiliary = {}

---------------------------------------------------------------------------------------------------------------
-- Lua object functions ---------------------------------------------------------------------------------------

function SetParent(object, parent)
	if object.Parent then
		local byName = object.Parent.ChildrenByName
		for k, v in pairs(byName) do
			if v==object then
				byName[k] = nil
			end
		end

		local byNumber = object.Parent.ChildrenByNumber
		for i=#byNumber, 1, -1 do
			if byNumber[i] == object then
				table.remove(byNumber, i)
			else
				if byNumber[i].Name == object.Name then
					byName[object.Name] = byNumber[i]
				end
			end
		end
	end

	object.Parent = parent

	if parent then
		table.insert(parent.ChildrenByNumber, object)

		if not parent.ChildrenByName[object.Name] then
			parent.ChildrenByName[object.Name] = object
		end
	end
end

function SetName(object, newName)
	local oldName = object.Name

	if object.Parent then
		local byName = object.Parent.ChildrenByName
		local byNumber = object.Parent.ChildrenByNumber

		if byName[oldName] == object then
			byName[oldName] = nil

			for i=#byNumber, 1, -1 do
				if byNumber[i] ~= object then
					if byNumber[i].Name == object.Name then
						byName[oldName] = byNumber[i]
					end
				end
			end
		end

		if byName[newName] == nil then
			byName[newName] = object
		end
	end

	object.Name = newName
end

---------------------------------------------------------------------------------------------------------------
-- Run function -----------------------------------------------------------------------------------------------

-- Runs in a coroutine, but catches errors. Should be used by default!
function auxiliary.Run(f, ...)
	if type(f) ~= "function" then
		print("glib.run: wrong argument type (should be function)")
	end
	--f(...)

	coroutine.resume(coroutine.create(
		function(...)
			xpcall(f,
				function(error)
					print("=================================================")
					--print("Lua error: " .. tonumber(debug.getinfo(1).currentline) .. error .. "\n", debug.traceback())
					print("Lua error: " .. error .. "\n", debug.traceback(nil, 2))
					print("=================================================")
				end,
				...
				)
		end
	), ...)

	--coroutine.resume(coroutine.create(f), ...)
	--collectgarbage("collect")
end

---------------------------------------------------------------------------------------------------------------
-- Object Orientation -----------------------------------------------------------------------------------------

	-- Basic, rudimentary but functional OO

local OOBehavior = {}
local OO = {}

local OOMetatable = {
	__index = function(self, key)
		return OOBehavior[key]
	end
}

function OOBehavior:inherit()
	local newClass = {}
	local newMetatable = {}

	-- Creating the class metatable, which is useful for telling classes apart

	for key, value in pairs(getmetatable(self)) do
		newMetatable[key] = value
	end

	-- Importing methods (variables should always be created on instantiation)

	for key, value in pairs(self) do
		newClass[key] = value
	end

	newClass.OOParent = self
	setmetatable(newClass, newMetatable)
	return newClass
end

function OOBehavior:sameType(comp)
	return ( getmetatable(self).Instantiated and getmetatable(getmetatable(self).__index) or getmetatable(self) ) ==
	( getmetatable(comp).Instantiated and getmetatable(getmetatable(comp).__index) or getmetatable(comp) )
end

function OOBehavior:super(...)
	self.OOParent.constructor(self, ...)
end

function OOBehavior:new(...)
	local newObject = {}

	local metatable = {}

	for k, v in pairs(getmetatable(self)) do
		metatable[k] = v
	end

	metatable.__index = self
	metatable.Instantiated = true

	setmetatable(newObject, metatable)
	-- setmetatable(newObject, {__index = self, Instantiated = true})
	newObject:constructor(...)

	return newObject
end

setmetatable(OO, OOMetatable)
auxiliary.OO = OO

---------------------------------------------------------------------------------------------------------------
-- Events -----------------------------------------------------------------------------------------------------

local Event = OO:inherit()

function Event:constructor()
	self.connections = {}
	-- setmetatable(self.connections, {__mode = "kv"}) can
	-- make keys or values, respectively, weak. Earlier versions
	-- had it enabled. But it could possibly create a few obstacles. (?!)
end

function Event:fire(...)
	for _, f in pairs(self.connections) do
		f(...)
	end
end

function Event:fireParallel(...)
	for _, f in pairs(self.connections) do
		glib.Run(f, ...)
	end
end

function Event:connect(f)
	if type(f) ~= "function" then
		error("Tried to connect unknown type.")
	end

	table.insert(self.connections, f)

	return {
		disconnect = function()
			for index, value in pairs(self.connections) do
				if value == f then
					self.connections[index] = nil
					break
				end
			end
		end,
		fire = f
	}
end

function Event:purgeConnections()
	self.connections = {}
end

auxiliary.Event = Event

---------------------------------------------------------------------------------------------------------------
-- Sealing glib -----------------------------------------------------------------------------------------------

local glib_meta = {
	__index = function(t, k)
		return auxiliary[k]
	end,
	__newindex = function()
	end,
	__metatable = {}
}

setmetatable(glib, glib_meta)
