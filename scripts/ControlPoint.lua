-- Copyright 2016
--
-- 	Authors:
-- 		Caio de Almeida Guimarães
-- 		Denilson Marques Junior
-- 		Lucas Eduardo Carreiro de Mello
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--  http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local Vector = require("./scripts/Vector")

local ControlPoint = glib.OO:inherit()

local CaptureDistance = 20

function countk(t)
	local count = 0
	for k, v in pairs(t) do
		count = count + 1
	end

	return count
end

local perlin = Perlin()

function ControlPoint:constructor(pos)
	self.Position = pos
	self.Owner = nil

	self.Capture = {}
	self.captureParticles = {}

	local px, py, pz = pos:unpack()

	-- Explode(x, y, z, size, radius, timeToWait, [initialColor, endColor, forever?, angle, smoke?])
	self.Particles = Particles.Explode(px, py+4, pz, 2, 1, 3, 240, 240, 240, 200, 200, 200, true, 0, false)

	self.Light = Light()
	self.Light:SetLightType(Enum.LightType.Point)
	self.Light:SetPosition(px, py+5, pz)
	self.Light:SetDiffuseColor(1, 1, 1)
	self.Light:SetRadius(20)

	self.Firepit = Model("assets/models/firepit.obj")
	self.Firepit:SetPosition(px, py+2, pz)
	-- self.Firepit:SetSize(10, 10, 10)
	self.Firepit:SetParent(shared.workspace)

	glib.Run(
		function()
			local r = math.random()
			repeat

				local t = tick()
				self.Light:SetRadius(0.5 + (perlin:Noise(t*2, r*255, 0)*6 + perlin:Noise(t*6, r*255, 0)*2))

				wait()
			until false
		end
	)

	glib.Run(
		function()
			wait(1)

			repeat
				local hasDefender = false
				local capturingTeams = {}

				for k, v in pairs(shared.Tanks) do
					if (Vector:new(v.LocalTank.body:GetPosition()) - self.Position):mag2() < CaptureDistance*CaptureDistance then
						capturingTeams[v.Team] = true

						if self.Owner == v.Team then
							hasDefender = true
						else
							self.Capture[v.Team] = (self.Capture[v.Team] or 0) + 5
						end
					end
				end

				if hasDefender or countk(capturingTeams) > 1 then
					for k, v in pairs(self.Capture) do
						self.Capture[k] = 0

						if self.captureParticles[k] ~= -1 and self.captureParticles[k] ~= nil then
							Particles.Destroy(self.captureParticles[k])
							self.captureParticles[k] = -1
						end
					end
				end

				local winner
				local max = 0

				for k, v in pairs(self.Capture) do
					if v > 0 then
						if self.captureParticles[k] == -1 or self.captureParticles[k] == nil then
							local r1, g1, b1 = k.initialColor:unpack()
							local r2, g2, b2 = k.endColor:unpack()
							self.captureParticles[k] = Particles.Explode(px, py+4, pz, 1, 0.5, 2, r1, g1, b1, r2, g2, b2, true, 0, false)
						end
					end

					if v >= 15 and v > max then
						max = v
						winner = k
					end
				end

				if winner then
					for k, v in pairs(self.Capture) do
						if self.captureParticles[k] ~= -1 and self.captureParticles[k] ~= nil then
							Particles.Destroy(self.captureParticles[k])
						end
					end

					self.Capture = {}
					self.captureParticles = {}
					self.Owner = winner

					-- print("Captured!!!")

					local r1, g1, b1 = winner.initialColor:unpack()
					local r2, g2, b2 = winner.endColor:unpack()

					Particles.ChangeColor(self.Particles, r1, g1, b1, r2, g2, b2)
				end

				wait(5)
			until false
		end
	)
end

return ControlPoint
