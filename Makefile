SRCDIR = src
OBJDIR = obj

PROGRAM = TankGame

CFLAGS = -std=c++11 -I./include -I./include/bullet -I./include/lua -O2
lib= -Llib -llua -ldl -lIrrlicht -lGL -lXxf86vm -lXext -lX11 -lBulletDynamics -lBulletCollision -lLinearMath -Wl,-rpath=./lib
CXX = g++

SRC = $(wildcard $(SRCDIR)/*.cpp)
OBJ = $(SRC:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEP = $(OBJ:.o=.d)

CFLAGS += -MMD -MP

.PHONY: all clean

all: bin/ bin/$(PROGRAM)

bin/: obj/
	mkdir -p bin/

obj/:
	mkdir -p obj/

bin/$(PROGRAM): $(OBJ)
	$(CXX) $^ -o $@ $(lib)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CFLAGS) -o $@ -c $<

-include $(DEP)

clean:
	rm -f $(OBJDIR)/* bin/$(PROGRAM)

run: all
	./bin/$(PROGRAM)

valgrind: all
	@valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes ./bin/$(PROGRAM)

valgrind2: all
	@valgrind --track-origins=yes ./bin/TankGame &> out
