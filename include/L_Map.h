///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _MAP
#define _MAP

#include <iostream>
#include <vector>

#include <irrlicht/irrlicht.h>
#include <math.h>

#include "Globals.h"
#include "L_Object.h"
#include "MotionState.h"
#include "B_Initializer.h"

using namespace irr;
using namespace std;

class L_Map : public L_Object{
	private:
	public:
		// C++ constructors

		L_Map();
		~L_Map();

		// Lua side constructors

		L_Map(lua_State * S);

		// Post-constructor / Pre-destructor

		void InitializeObject(lua_State *L);
		void DestroyObject(lua_State *L);

		// Class methods and attributes

		struct Triangle{
			btVector3 vx1, vx2, vx3;
		};

		vector<Triangle> triangles;

		vector<scene::ISceneNode *> sceneNodes;
		btRigidBody * rigidBody;

		bool built;

		// Lua binded methods

		int L_AddModel(lua_State *L);
		int L_AddModelSegmented(lua_State *L);
		int L_AddOBJ(lua_State *L);

		int L_Build(lua_State *L);

		// Lua binding configuration

	  	static const char className[];
		static Luna<L_Map>::RegType Register[];

		// Events

};

void rescaleShapeT(vector<tinyobj::shape_t> & obj, float scale);

extern const int maxPolys;

#endif
