///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef PROJETOCG_PARTICLES_H
#define PROJETOCG_PARTICLES_H

#include <irrlicht/irrlicht.h>
#include <vector>
#include <map>

#include "luna.h"
#include "GameTimer.h"
#include "Globals.h"

using namespace irr;

class Particles
{
private:
    Particles();
    static std::vector<std::pair<GameTimer*, scene::IParticleSystemSceneNode*>> timers;
    static std::map<int, scene::IParticleSystemSceneNode*> particleNodes;
    static int particleNodesCounter;

public:
    static void initializeParticles(lua_State *L);

    static int explosionParticles(lua_State *L);

    static int destroyParticles(lua_State *L);

    static int changeColor(lua_State *L);

    static void update();
};

#endif //PROJETOCG_PARTICLES_H
