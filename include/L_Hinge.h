///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _HINGE
#define _HINGE

#include "L_Object.h"
#include "B_Initializer.h"

class L_Hinge : public L_Object{
	private:
	public:
		// C++ constructors

		L_Hinge();
		~L_Hinge();

		// Lua side constructors

		L_Hinge(lua_State * L);

		// Post-constructor / Pre-destructor

		void InitializeObject(lua_State *L);
		void DestroyObject(lua_State *L);

		// Class methods and attributes

		btHingeConstraint *hingeConstraint;

		// Lua binded methods
		int L_Destroy(lua_State *L);
        int L_SetMotor(lua_State *L);
        int L_DisableMotor(lua_State *L);
       	int L_SetLimits(lua_State *L);
       	int L_GetAngle(lua_State *L);
       	int L_SetAngleMotor(lua_State *L);
       	int L_SetAngleMotorTarget(lua_State *L);

		// Lua binding configuration

	  	static const char className[];
		static Luna<L_Hinge>::RegType Register[];

		// Events

};

#endif
