///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef PROJETOCG_TIME_H
#define PROJETOCG_TIMER_H

#include "Globals.h"

class GameTimer
{
public:
   GameTimer();
   ~GameTimer();

   s32 getSecondsElapsed();
   f32 getMinutesElapsed();
   f32 getDeltaTime();
   bool timeHasPassed();

   void init(ITimer* t, f32 timeToWait = 0);
   void update();

private:

   s32 Seconds;
   f32 Minutes;

   f32 Delta;
   u32 NewTime;

   u32 OldTime;
   f32 Elapsed;
   u32 Counter;

   f32 timeToWait;


   ITimer* timer;
};

#endif
