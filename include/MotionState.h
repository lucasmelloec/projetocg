///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _MOTIONSTATE
#define _MOTIONSTATE

#include <irrlicht/irrlicht.h>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>

#include <iostream>

#include "tiny_obj_loader.h"

using namespace std;
using namespace irr;

class gameMotionState : public btMotionState
{
protected:
    btTransform mInitialPosition;

public:
    vector<pair<scene::ISceneNode *, btTransform> > sceneNodes;

    gameMotionState(const btTransform &initialPosition);

    virtual ~gameMotionState();

    virtual void getWorldTransform(btTransform &worldTrans) const;

    virtual void setWorldTransform(const btTransform &worldTrans);

    void setKinematicPosition(const btTransform & worldTrans);

    void addSceneNode(scene::ISceneNode * node, btTransform transf);
    void addSceneNode(scene::ISceneNode * node);
    void removeSceneNode(scene::ISceneNode * node);
};

void QuaternionToEuler(const btQuaternion &TQuat, btVector3 &TEuler);

#endif
