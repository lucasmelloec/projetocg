///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef __INPUTHANDLER
#define __INPUTHANDLER

#include <irrlicht/irrlicht.h>
#include <iostream>
#include <queue>

#include "luah.h"
#include "LuaEventWrapper.h"

using namespace std;
using namespace irr;

union EventUnion{
	struct {
		bool down;
		int key;
	} Key;
	struct {
		int x, y;
	} MousePos;
};

class InputHandler : public IEventReceiver
{
	public:

	lua_State * mainState;

	LuaEvent * OnKeyDown;
	LuaEvent * OnKeyUp;

	LuaEvent * MouseButton1Down;
	LuaEvent * MouseButton1Up;

	LuaEvent * MouseButton2Down;
	LuaEvent * MouseButton2Up;

	LuaEvent * MouseMoved;

	queue<pair<LuaEvent *, EventUnion> > EventQueue;

	// This is the one method that we have to implement
	virtual bool OnEvent(const SEvent& event);

	// This is used to check whether a key is being held down
	virtual bool IsKeyDown(EKEY_CODE keyCode) const;

	InputHandler();

	void Destroy(lua_State * L);

	// Lua stuff

	void initializeLua(lua_State * L);

	void handleEvents(lua_State * L);

	private:

	// We use this array to store the current state of each key
	bool KeyIsDown[KEY_KEY_CODES_COUNT];
};

extern InputHandler inputHandler;

#endif
