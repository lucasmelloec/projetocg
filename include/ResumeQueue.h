///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _RESUMEQUEUE
#define _RESUMEQUEUE

#include <queue>
#include <vector>

#include "luna.h"

#include "Globals.h"

using namespace std;

class ResumeWait{
	private:

	public:
		priority_queue<pair<float, int>, vector<pair<float, int> >, std::greater<pair<float, int> > > toResume;

		ResumeWait();

		void ResumeAll(lua_State * L);
};

class ResumeEvent{
	private:

	public:
		queue<lua_State *> toResume;

		ResumeEvent();

		void ResumeAll(lua_State * L);
};

extern ResumeWait WaitQueue;

void report_errors(lua_State *L, int status);

#endif
