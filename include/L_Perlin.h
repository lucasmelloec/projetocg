///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _SCENE
#define _SCENE

#include <irrlicht/irrlicht.h>

#include "L_Object.h"
#include "PerlinNoise.h"

using namespace irr;

class L_Perlin : public L_Object{
	private:
	public:
		// C++ constructors

		L_Perlin();
		virtual ~L_Perlin();

		// Lua side constructors

		L_Perlin(lua_State * S);

		// Post-constructor / Pre-destructor

		void InitializeObject(lua_State *L);
		void DestroyObject(lua_State *L);

		// Class methods and attributes

		PerlinNoise perlin;

		// Lua binded methods
		int L_Noise(lua_State *L);

		// Lua binding configuration

	  	static const char className[];
		static Luna<L_Perlin>::RegType Register[];

		// Events

};

#endif
