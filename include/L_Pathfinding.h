///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _PATHFINDING
#define _PATHFINDING

#include <irrlicht/irrlicht.h>
#include <math.h>

#include "Globals.h"
#include "L_Model.h"
#include "L_Object.h"
#include "B_Initializer.h"
#include "MotionState.h"
#include "L_Map.h"

class Polygon{
public:
	int size;
	core::vector3df * vx = NULL;

	int id;

	float minX, minZ, maxX, maxZ;
	bool hasBounds = false;

	Polygon(){};
	Polygon(int n){
		vx = (core::vector3df *)malloc(n*sizeof(core::vector3df));
		size = n;

		for(int i=0; i<n; i++){
			vx[i] = core::vector3df(0, 0, 0);
		}
	};
	~Polygon(){
		if(vx!=NULL)
			free(vx);
	}

	void calculateBounds();
	bool isIn(const float x, const float y);
	bool isIn(const core::vector3df & v);

	core::vector3df getCenter();
	core::vector3df getCenterXZ();
	core::vector3df fastCenter();

};

class Triangle : public Polygon{
public:
	core::vector3df &vx1, &vx2, &vx3;

	Triangle() : Polygon(3), vx1(vx[0]), vx2(vx[1]), vx3(vx[2]){}

	float getDistance(Triangle * t);
};

class L_Pathfinding : public L_Object{
	private:
	public:
		// C++ constructors

		L_Pathfinding();
		~L_Pathfinding();

		// Lua side constructors

		L_Pathfinding(lua_State * S);

		// Post-constructor / Pre-destructor

		void InitializeObject(lua_State *L);
		void DestroyObject(lua_State *L);

		// Class methods and attributes

		// class Triangle{
		// 	public:
		// 	Triangle();

		// 	int id;
		// 	core::vector3df vx1, vx2, vx3;

		// 	core::vector3df getCenter();
		// };

		vector<vector<int> > G;
		std::map<core::vector3df, vector<Triangle *> > pmap;
		vector<Triangle *> triangles;

		int currentId = 0;

		// Polygon * MatchPolygonEdge(core::vector3df vx1, core::vector3df vx2);
		Triangle * MatchTriangleEdge(core::vector3df vx1, core::vector3df vx2);

		int gridSize = 12;

		float gridStartX, gridStartZ;
		float gridSizeX, gridSizeZ;
		float gridStepX, gridStepZ;

		vector<Triangle *> ** grid = NULL;

		pair<int, int> getGridFromPoint(float x, float y);
		vector<Triangle *> * getGridContent(float x, float y);
		int makeGrid();

		Triangle * TriangleFromPoint(float x, float y);

		Triangle * GetNearestTriangle(float x, float y);

		// Lua binded methods

		int L_AddNavMesh(lua_State *L);
		int L_GetWaypoints(lua_State *L);
		int L_GetNearestTriangleDistance(lua_State *L);

		// Lua binding configuration

	  	static const char className[];
		static Luna<L_Pathfinding>::RegType Register[];

		// Events

};

#endif
