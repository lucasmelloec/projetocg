///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _GLOBALS
#define _GLOBALS

#include <chrono>
#include <iostream>
#include <irrlicht/irrlicht.h>

#include "luah.h"

#define PROFILE_LOOP false

using namespace irr;
using namespace std;

class Timer
{
public:
    Timer() : beg_(clock_::now()) {}
    void reset() { beg_ = clock_::now(); }
    double elapsed() const {
        return std::chrono::duration_cast<second_>
            (clock_::now() - beg_).count(); }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};

extern Timer globalTimer;
extern int objstoreCurrent;

extern scene::ISceneManager * irrmanager;
extern video::IVideoDriver * irrdriver;
extern IrrlichtDevice * device;

const float pi = 3.1415926535;

int LuaRun(lua_State * L);
int LuaRun(lua_State * L, const char * filename);
int LuaRun2(lua_State * L);
int LuaRun2(lua_State * L, const char * filename);

#endif
