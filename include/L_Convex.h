///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _CONVEX
#define _CONVEX

#include <irrlicht/irrlicht.h>
#include <math.h>

#include "Globals.h"
#include "L_Model.h"
#include "L_Object.h"
#include "B_Initializer.h"
#include "MotionState.h"

using namespace irr;

class L_Convex : public L_Object{
	private:
	public:
		static map<btRigidBody *, L_Convex *> rigidMap;

		// C++ constructors

		L_Convex();
		~L_Convex();

		// Lua side constructors

		L_Convex(lua_State * S);

		// Post-constructor / Pre-destructor

		void InitializeObject(lua_State *L);
		void DestroyObject(lua_State *L);

		// Class methods and attributes

		btRigidBody * rigidBody;
		double bodyMass;

		vector<btHingeConstraint *> connectedParts;

		LuaEvent *collisionDetected;

		// Lua binded methods

		//int L_SetDiffuseColor(lua_State *L);
		//int L_SetSpecularColor(lua_State *L);

		int L_SetPosition(lua_State *L);
		int L_GetPosition(lua_State *L);
		int L_SetRotation(lua_State *L);
		int L_SetSize(lua_State *L);
		int L_SetVelocity(lua_State *L);
		int L_SetAngularVelocity(lua_State *L);
		int L_SetGravity(lua_State *L);

		int L_SetMass(lua_State *L);

		int L_AddModel(lua_State *L);

		int L_ToWorld(lua_State *L);
		int L_ToObject(lua_State *L);

		int L_ApplyImpulse(lua_State *L);

		// Lua binding configuration

	  	static const char className[];
		static Luna<L_Convex>::RegType Register[];

		// Events
		void collided(lua_State *L);
};

#endif
