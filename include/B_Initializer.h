///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _B_INITIALIZER
#define _B_INITIALIZER

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include <iostream>
#include <set>
#include <map>

using namespace std;

enum collisionClasses {
	COL_NOTHING = 	0,
	COL_DEFAULT = 	1<<0,
	COL_NOCOLLIDE = 1<<1
};

class BulletPhysics{
	private:
	BulletPhysics(){};

	public:
	~BulletPhysics(){};

	static btBroadphaseInterface * broadphase;
	static btDefaultCollisionConfiguration * collisionConfiguration;
	static btCollisionDispatcher * dispatcher;
	static btSequentialImpulseConstraintSolver * solver;
	static btDiscreteDynamicsWorld * dynamicsWorld;
};

void initializeBullet();
void finalizeBullet();

#endif
