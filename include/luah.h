///*      Copyright 2016
//
//				Authors:
//					Caio de Almeida Guimarães
//					Denilson Marques Junior
//					Lucas Eduardo Carreiro de Mello
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License. */
//

#ifndef _LUAH
#define _LUAH

// All C++ functions that are registered in Lua must check for errors if
// LUA_CHECK_ERRORS is true.
#define LUA_CHECK_ERRORS true

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}


#endif
